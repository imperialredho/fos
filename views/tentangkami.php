<?php partial('head') ?>
	<div class="container login-background">
		<div class="row">
			<?php if (flashExists('error')): ?>
			<div class="alert alert-danger">
				<?php echo flash('error') ?>
			</div>
		<?php endif ?>
			<div class="col-1"></div>
			<div class="col content">
				<h4>Tentang Kami</h4>
				<div class="row">
					<img class="col-6 col-sm-6"src="<?php echo asset('img/tk-1.jpg')?>" alt="" >
					<img class="col-6 col-sm-6"src="<?php echo asset('img/tk-2.jpg')?>" alt="" >
				</div>
				<div>Nasi Goreng Padang Arul yang berlokasi di jalan Masjid Al Anwar Sukabumi Utara Kebon Jeruk. Yang mana kami mampu bersaing dengan bisnis lain yang ada dikawasan kebon jeruk yaitu kami menyediakan berbagai macam menu Nasi goreng dan lain - lain. Yang diolah dengan bumbu - bumbu tradisional sehingga mampu menciptakan citarasa yang khas bagi pelanggan setia kami.</div>
				<h5>Anda dapat menghubungi kami di : 0813-1749-9039</h5>
			</div>
			<div class="col-1"></div>
		</div>
	</div>
<?php partial('tail') ?>
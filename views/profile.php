<?php partial('head', ['title' => 'Akun Saya']) ?>
<div class="container">
	<h1>Selamat datang, <?php echo getAuthUser()['name'] ?></h1>
	<div class="row d-flex justify-content-center">
		<h2 class="col-3">Nama</h2>
		<h2 class="col profile-content"><?php echo getAuthUser()['name'] ?></h4>
	</div>

	<div class="row d-flex justify-content-center">
		<h2 class="col-3">Email</h2>
		<h2 class="col profile-content"><?php echo getAuthUser()['email'] ?></h4>
	</div>
	<div class="row d-flex justify-content-center">
		<h2 class="col-3">No. Telepon</h2>
		<h2 class="col profile-content"><?php echo getAuthUser()['phone'] ?></h2>
	</div>
	<a class="btn btn-warning" href="<?php echo url('editprofile') ?>">Edit</a>


</div>
<?php partial('tail') ?>
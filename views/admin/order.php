
<?php partial('admin/nav', ['title' => 'Order']); ?>
<div id="wrapper">

  <!-- Sidebar -->
  <ul class="sidebar navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('admin') ?>">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('food') ?>">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Products</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('user') ?>">
        <i class="fas fa-fw fa-users"></i>
        <span>Users</span></a>
    </li>
    <li class="nav-item active">
      <a class="nav-link" href="<?php echo url('order') ?>">
        <i class="fas fa-fw fa-shopping-cart"></i>
        <span>Orders</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('hot') ?>">
        <i class="fab fa-hotjar"></i>
        <span>What's Hot</span></a>
    </li>
    <hr>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('') ?>">
        <i class="far fa-arrow-alt-circle-left"></i>
        <span>Back to main menu</span></a>
    </li>
  </ul>

  <div id="content-wrapper">

    <div class="container-fluid">

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="admin">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">Orders</li>
    </ol>

    <!-- table -->
    <div class="card mb-3">
      <div class="card-header">
        <i class="fas fa-table"></i>
        Data Order</div>
      <div class="row">
        <div class="card-body col-10">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Address</th>
                  <th>Email</th>
                  <th>Delivery Fee</th>
                  <th>Total Fee</th>
                  <th>Note</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Date</th>
                  <th>Address</th>
                  <th>Email</th>
                  <th>Delivery Fee</th>
                  <th>Total Fee</th>
                  <th>Note</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>
                <?php foreach ($orders as $order):
                $idOrder = $order['id_order'];
                $created = $order['created_at'];
                $address = $order['address'];
                $email = $order['email'];
                $fee = $order['delivery_fee'];
                $fee2 = $order['total_fee'];
                $note = $order['note'];?>
                <tr>
                  <td><?php echo $created ?></td>
                  <td><?php echo $address?></td>
                  <td><?php echo $email  ?></td>
                  <td><?php echo $fee  ?></td>
                  <td><?php echo $fee2  ?></td>
                  <td><?php echo $note  ?></td>
                  <td><button type="button" class="btn btn-danger fas fa-trash-alt product-delete" data-toggle="modal" data-target="#buttonDelete" data-id="<?php echo $idOrder ?>"></button>
                    <button type="button" class="btn btn-info fas fa-edit product-update"
                    data-toggle="modal"
                    data-target="#updateMenuModal"
                    data-id="<?php echo $idOrder ?>"
                    data-created = "<?php echo $created ?>"
                    data-address = "<?php echo $address ?>"
                    data-email = "<?php echo $email ?>"
                    data-fee = "<?php echo $fee ?>"
                    data-fee2 = "<?php echo $fee2 ?>"
                    data-note = "<?php echo $note ?>">
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>

      <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>

      <!-- update -->
      <div class="modal fade" id="updateMenuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Order</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php echo url('admin/order/update') ?>" method="post" id="insert-update">
            <div class="form-group">
              <label for="inputDate">Date</label>
              <input readonly value="" name="update_price" type="text" class="form-control-plaintext" id="inputDate" aria-describedby="emailHelp" placeholder="update price" required>
            </div>
            <div class="form-group">
              <label for="inputAddress">Address</label>
              <input value="" name="update_address" type="text" class="form-control" id="inputAddress" placeholder="update menu" required>
            </div>
            <div class="form-group">
              <label for="inputEmail">Email</label>
              <input value="" readonly name="update_price" type="text" class="form-control-plaintext" id="inputEmail" aria-describedby="emailHelp" placeholder="update email" required>
            </div>
            <div class="form-group">
              <label for="inputFee">Delivery Fee</label>
              <input value="" name="update_fee" type="text" class="form-control" id="inputFee" placeholder="update fee" required>
            </div>
            <div class="form-group">
              <label for="inputFee2">Total Fee</label>
              <input value="" name="update_fee2" type="text" class="form-control" id="inputFee2" placeholder="update total" required>
            </div>
            <div class="form-group">
              <label for="inputNote">Note</label>
              <input value="" name="update_note" type="text" class="form-control" id="inputNote" placeholder="update note" required>
            </div>
            <input id="order-id-update" type="hidden" name="id_order" value="">
          </form>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-primary" type="submit" form="insert-update">Update</button>
        </div>
      </div>
      </div>
      </div>

      <!-- hapus -->
      <div class="modal fade" id="buttonDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Order?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <form class="" action="<?php echo url('admin/order/delete') ?>" method="post">
            <input id="order-id" type="hidden" name="id_order" value="">
          <button class="btn btn-primary" type="submit">Hapus</button>
        </form>
        </div>
      </div>
      </div>
      </div>
</div>

<?php partial('admin/footer', ['js' => ['admin/order']]) ?>

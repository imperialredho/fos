<?php partial('admin/nav', ['title' => 'Profile']); ?>
<div id="wrapper">

  <!-- Sidebar -->
  <ul class="sidebar navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('admin') ?>">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('food') ?>">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Products</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('user') ?>">
        <i class="fas fa-fw fa-users"></i>
        <span>Users</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('order') ?>">
        <i class="fas fa-fw fa-shopping-cart"></i>
        <span>Orders</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('hot') ?>">
        <i class="fab fa-hotjar"></i>
        <span>What's Hot</span></a>
    </li>
    <hr>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('') ?>">
        <i class="far fa-arrow-alt-circle-left"></i>
        <span>Back to main menu</span></a>
    </li>
  </ul>

  <div id="content-wrapper">

    <div class="container-fluid">

<!-- Breadcrumbs-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="admin">Dashboard</a>
  </li>
  <li class="breadcrumb-item active">Profile</li>
</ol>

<?php if (flashExists('error')): ?>
  <div class="alert alert-danger">
    <?php echo flash('error') ?>
  </div>
<?php endif ?>
</ol>

<?php if (flashExists('success')): ?>
  <div class="alert alert-info">
    <?php echo flash('success') ?>
  </div>
<?php endif ?>

<div class="col-md-9">
		    <div class="card">
		        <div class="card-body">
		            <div class="row">
		                <div class="col-md-12">
		                    <h4>Your Profile</h4>
		                    <hr>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-md-12">
		                    <form action="<?php echo url('admin/profile/update') ?>" method="post" id="insert-profile">
                              <div class="form-group row">
                                <label for="username" class="col-4 col-form-label">Name*</label>
                                <div class="col-8">
                                  <input id="username" value="<?php echo getAuthUser()['name'] ?>" name="username" placeholder="Username" class="form-control here" required="required" type="text">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="email" class="col-4 col-form-label">Email*</label>
                                <div class="col-8">
                                  <input id="email" value="<?php echo getAuthUser()['email'] ?>" name="email" placeholder="Email" class="form-control-plaintext here" readonly required="required" type="text">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="website" class="col-4 col-form-label">Phone*</label>
                                <div class="col-8">
                                  <input id="website"value="<?php echo getAuthUser()['phone'] ?>" name="phone" placeholder="phone" class="form-control here" type="text" required>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="pass" class="col-4 col-form-label">Password</label>
                                <div class="col-8">
                                  <input id="pass" value="" name="password" placeholder="Password" class="form-control here" type="password" required>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="newpass" class="col-4 col-form-label">New Password</label>
                                <div class="col-8">
                                  <input id="newpass" value="" name="newpassword" placeholder="New Password" class="form-control here" type="password" required>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="newpass" class="col-4 col-form-label">Confirm New Password</label>
                                <div class="col-8">
                                  <input id="newpass" value="" name="newpasswordconfirm" placeholder="Confirm New Password" class="form-control here" type="password" required>
                                </div>
                              </div>
                              <div class="form-group row">
                                <div class="offset-4 col-8">
                                  <button name="submit" type="submit" class="btn btn-primary" form="insert-profile">Update My Profile</button>
                                </div>
                              </div>
                          </form>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
<?php partial('admin/footer', ['js' => ['admin/profile']]); ?>

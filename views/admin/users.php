
<?php partial('admin/nav', ['title' => 'Users']) ?>

<div id="wrapper">

  <!-- Sidebar -->
  <ul class="sidebar navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('admin') ?>">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('food') ?>">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Products</span></a>
    </li>
    <li class="nav-item active">
      <a class="nav-link" href="<?php echo url('user') ?>">
        <i class="fas fa-fw fa-users"></i>
        <span>Users</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('order') ?>">
        <i class="fas fa-fw fa-shopping-cart"></i>
        <span>Orders</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('hot') ?>">
        <i class="fab fa-hotjar"></i>
        <span>What's Hot</span></a>
    </li>
    <hr>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('') ?>">
        <i class="far fa-arrow-alt-circle-left"></i>
        <span>Back to main menu</span></a>
    </li>
  </ul>

  <div id="content-wrapper">

    <div class="container-fluid">

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="admin">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">Users</li>
    </ol>

    <?php if (flashExists('error')): ?>
      <div class="alert alert-danger">
        <?php echo flash('error') ?>
      </div>
    <?php endif ?>

    <!-- table -->
    <div class="card mb-3">
      <div class="card-header">
        <button type="button" class="btn btn-secondary float-right" data-toggle="modal" data-target="#tambahUserModal">Tambah User</button>
        <i class="fas fa-table"></i>
        Data Users</div>
      <div class="row">
        <div class="card-body col-10">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Password</th>
                  <th>Phone</th>
                  <th>Privilege</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Password</th>
                  <th>Phone</th>
                  <th>Privilege</th>
                  <th>Action</th>
              </tfoot>
              <tbody>
                <?php foreach ($users as $user):
                  $name = $user['name'];
                  $email = $user['email'];
                  $pass = $user['password'];
                  $phone = $user['phone'];
                  $pri = $user['privilege'];?>
                <tr>
                  <td><?php echo $name ?></td>
                  <td><?php echo $email ?></td>
                  <td><?php echo $pass ?></td>
                  <td><?php echo $phone ?></td>
                  <td><?php if ($pri == '1'):
                    echo "Admin";
                    elseif ($pri == '2'):
                      echo "User";
                      else :
                        echo "Undetected";
                  endif; ?></td>
                  <td><button type="button" class="btn btn-danger fas fa-trash-alt user-delete" data-toggle="modal" data-target="#buttonDelete" data-id="<?php echo $user['email'] ?>"></button>
                    <button type="button" class="btn btn-info fas fa-edit user-update"
                    data-toggle="modal"
                    data-target="#updateMenuModal"
                    data-id="<?php echo $email ?>"
                    data-name="<?php echo $name ?>"
                    data-password="<?php echo $pass ?>"
                    data-phone="<?php echo $phone ?>"
                    data-privilege="<?php echo $pri ?>">
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>

      <!-- Tambah Modal-->
      <div class="modal fade" id="tambahUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah User</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="post" action="<?php echo url('admin/user/insert') ?>" id="insert-user-form">
            <div class="form-group">
              <label for="inputName">Name</label>
              <input name="name" type="text" class="form-control" id="inputName" placeholder="Your name" required>
            </div>
            <div class="form-group">
              <label for="inputEmail1">Email address</label>
              <input name="email" type="email" class="form-control" id="inputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
              <label for="inputPassword1">Password</label>
              <input name="password" type="password" class="form-control" id="inputPassword1" placeholder="Password" required>
            </div>
            <div class="form-group">
              <label for="inputPhone">Phone</label>
              <input name="phone" type="text" class="form-control" id="inputPhone" placeholder="0821...." required>
            </div>
            <div class="form-group">
              <label for="formControlSelect1">Privelege</label>
              <select name="privilege" class="form-control" id="formControlSelect1">
                <option value="1">Admin</option>
                <option value="2">User</option>
              </select>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-primary"  type="submit" form="insert-user-form">Tambah</button>
        </div>
      </div>
      </div>
      </div>

      <!-- update -->
      <div class="modal fade" id="updateMenuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update User</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php echo url('admin/user/update') ?>" method="post" id="insert-update">
            <div class="form-group">
              <label for="inputName">Username</label>
              <input id="product-name" value="" name="update_name" type="text" class="form-control" id="inputName" placeholder="update name" required>
            </div>
            <div class="form-group">
              <label for="inputEmail1">Email address</label>
              <input id="product-email" value="" name="update_email" type="text" class="form-control-plaintext" readonly id="inputEmail1" aria-describedby="emailHelp" placeholder="update email">
            </div>
            <div class="form-group">
              <label for="inputPass">Password</label>
              <input id="product-pass" value="" name="update_password" type="password" class="form-control" id="inputPass" aria-describedby="emailHelp" placeholder="update password" required>
            </div>
            <div class="form-group">
              <label for="inputPhone">Phone</label>
              <input id="product-phone" value="" name="update_phone" type="text" class="form-control" id="inputPhone" placeholder="0821...." required>
            </div>
            <div class="form-group">
              <label for="formControlSelect1">Privelege</label>
              <select id="product-pri" value="" name="update_privilege" class="form-control" id="formControlSelect1">
                <option value="1">Admin</option>
                <option value="2">User</option>
              </select>
            </div>
            <input id="product-id-update" type="hidden" name="id_product2" value="">
          </form>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-primary" type="submit" form="insert-update">Update</button>
        </div>
      </div>
      </div>
      </div>

      <!-- hapus -->
      <div class="modal fade" id="buttonDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus User?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <form class="" action="<?php echo url('admin/user/delete') ?>" method="post">
            <input id="user-email-key" type="hidden" name="user_key" value="">
          <button class="btn btn-primary" type="submit">Hapus</button>
        </form>
        </div>
      </div>
      </div>
      </div>

<script src="<?php echo asset('js/admin/user.js') ?>"></script>
<?php partial('admin/footer', ['js' => ['admin/profile']]) ?>

<?php partial('admin/nav', ['title' => 'Products']); ?>
<div id="wrapper">

  <!-- Sidebar -->
  <ul class="sidebar navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('admin') ?>">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="nav-item active">
      <a class="nav-link" href="<?php echo url('food') ?>">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Products</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('user') ?>">
        <i class="fas fa-fw fa-users"></i>
        <span>Users</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('order') ?>">
        <i class="fas fa-fw fa-shopping-cart"></i>
        <span>Orders</span></a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="<?php echo url('hot') ?>">
        <i class="fab fa-hotjar"></i>
        <span>What's Hot</span></a>
    </li>
    <hr>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('') ?>">
        <i class="far fa-arrow-alt-circle-left"></i>
        <span>Back to main menu</span></a>
    </li>
  </ul>

  <div id="content-wrapper">

    <div class="container-fluid">

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="admin">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">Products</li>
    </ol>

    <!-- table -->
    <div class="card mb-3">
      <div class="card-header">
        <button type="button" class="btn btn-secondary float-right" data-toggle="modal" data-target="#tambahUserModal">Tambah Menu</button>
        <i class="fas fa-table"></i>
        Data Menu</div>
      <div class="row">
        <div class="card-body col-10">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th>Type</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th>Type</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
              <tbody>
                <?php foreach ($menus as $menu):
                $type = $menu['type'];
                $idProduct = $menu['id_product'];
                $name = $menu['name'];
                $desc = $menu['description'];
                $price = $menu['price'];
                $img = $menu['photo'];?>
                <tr>
                  <td><?php echo $name ?></td>
                  <td><?php echo $desc ?></td>
                  <td><?php echo $price ?></td>
                  <td><?php if ($type == '1'):
                    echo "Food";
                    elseif ($type == '2'):
                      echo "Drink";
                      else :
                        echo "Undetected";
                  endif; ?></td>
                  <td><button type="button" class="btn btn-danger fas fa-trash-alt product-delete" data-toggle="modal" data-target="#buttonDelete" data-id="<?php echo $idProduct ?>"></button>
                    <button type="button" class="btn btn-info fas fa-edit product-update"
                     data-toggle="modal"
                      data-target="#updateMenuModal"
                       data-id="<?php echo $idProduct ?>"
                       data-name="<?php echo $name ?>"
                       data-desc="<?php echo $desc ?>"
                       data-price="<?php echo $price ?>"
                       data-type="<?php echo $type?>"
                       data-img="<?php echo asset($img) ?>">
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>

      <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>

      <!-- Tambah Modal-->
      <div class="modal fade" id="tambahUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Menu</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="post" action="<?php echo url('admin/food/insert') ?>" id="insert-food-form" enctype="multipart/form-data">
            <div class="form-group">
              <label for="inputName">Menu</label>
              <input name="name" type="text" class="form-control" id="inputName" placeholder="New menu" required>
            </div>
            <div class="form-group">
              <label for="inputDesc">Description</label>
              <input name="desc" type="text" class="form-control" id="inputDesc" placeholder="New desc" required>
            </div>
            <div class="form-group">
              <label for="inputEmail1">Price</label>
              <input name="price" type="text" class="form-control" id="inputEmail1" aria-describedby="emailHelp" placeholder="Enter price" required>
            </div>
            <div class="form-group">
              <label for="formControlSelect1">Menu Type</label>
              <select name="type" class="form-control" id="formControlSelect1">
                <option value="1">Food</option>
                <option value="2">Drink</option>
              </select>
            </div>
            <div class="form-group">
              <label for="inputEmail1">Masukkan Gambar:</label>
              <input name="photo" type="file" class="form-control" id="inputEmail1" aria-describedby="emailHelp" placeholder="Enter price">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-primary" type="submit" form="insert-food-form">Tambah</button>
        </div>
      </div>
      </div>
      </div>

      <!-- update -->
      <div class="modal fade" id="updateMenuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Menu</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php echo url('admin/food/update') ?>" method="post" id="insert-update" enctype="multipart/form-data">
            <div class="form-group">
              <label for="inputName">Menu</label>
              <input id="product-menu" value="" name="update_name" type="text" class="form-control" id="inputName" placeholder="update menu" required>
            </div>
            <div class="form-group">
              <label for="inputName">Description</label>
              <input name="update_desc" type="text" class="form-control" id="update-desc" placeholder="update desc" required>
            </div>
            <div class="form-group">
              <label for="inputEmail1">Price</label>
              <input id="product-price" value="" name="update_price" type="text" class="form-control" id="inputEmail1" aria-describedby="emailHelp" placeholder="update price" required>
            </div>
            <div class="form-group">
              <label for="inputEmail1">Image</label>
              <div class="text-center">
                <img id="product-img" src="" style="width:50%">
              </div>
              <input type="file" name="photo" accept="image/*">
            </div>
            <div class="form-group">
              <label for="formControlSelect1">Menu Type</label>
              <select id="product-type" value="" name="update_type" class="form-control" id="formControlSelect1">
                <option value="1">Food</option>
                <option value="2">Drink</option>
              </select>
            </div>
            <input id="product-id-update" type="hidden" name="id_product2" value="">
          </form>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-primary" type="submit" form="insert-update">Update</button>
        </div>
      </div>
      </div>
      </div>

      <!-- hapus -->
      <div class="modal fade" id="buttonDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Menu?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <form class="" action="<?php echo url('admin/food/delete') ?>" method="post">
            <input id="product-id" type="hidden" name="id_product" value="">
          <button class="btn btn-primary" type="submit">Hapus</button>
        </form>
        </div>
      </div>
      </div>
      </div>
</div>
<?php partial('admin/footer', ['js' => ['admin/food']]) ?>

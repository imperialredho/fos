<?php partial('admin/nav', ['title' => 'Dashboard']) ?>
<div id="wrapper">

  <!-- Sidebar -->
  <ul class="sidebar navbar-nav">
    <li class="nav-item active">
      <a class="nav-link" href="<?php echo url('admin') ?>">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('food') ?>">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Products</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('user') ?>">
        <i class="fas fa-fw fa-users"></i>
        <span>Users</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('order') ?>">
        <i class="fas fa-fw fa-shopping-cart"></i>
        <span>Orders</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('hot') ?>">
        <i class="fab fa-hotjar"></i>
        <span>What's Hot</span></a>
    </li>
    <hr>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('') ?>">
        <i class="far fa-arrow-alt-circle-left"></i>
        <span>Back to main menu</span></a>
    </li>
  </ul>

  <div id="content-wrapper">

    <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>

          <!-- Icon Cards-->
          <div class="row">
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-primary o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-utensils"></i>
                  </div>
                  <div class="mr-5">Products!</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="<?php echo url('food') ?>">
                  <span class="float-left">View Details</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-warning o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-users"></i>
                  </div>
                  <div class="mr-5">Users!</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="<?php echo url('user') ?>">
                  <span class="float-left">View Details</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-success o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-shopping-cart"></i>
                  </div>
                  <div class="mr-5">Orders!</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="<?php echo url('order') ?>">
                  <span class="float-left">View Details</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-danger o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fab fa-hotjar"></i>
                  </div>
                  <div class="mr-5">What's Hot!</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="<?php echo url('hot') ?>">
                  <span class="float-left">View Details</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
          </div>

          <!-- Area Chart Example-->
          <!-- <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-chart-area"></i>
              Area Chart Example</div>
            <div class="card-body">
              <canvas id="myAreaChart" width="100%" height="30"></canvas>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
          </div> -->

<?php partial('admin/footer')?>

<?php partial('admin/nav', ['title' => 'What\'s Hot']); ?>
<div id="wrapper">

  <!-- Sidebar -->
  <ul class="sidebar navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('admin') ?>">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="<?php echo url('food') ?>">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Products</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('user') ?>">
        <i class="fas fa-fw fa-users"></i>
        <span>Users</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('order') ?>">
        <i class="fas fa-fw fa-shopping-cart"></i>
        <span>Orders</span></a>
    </li>
    <li class="nav-item active">
      <a class="nav-link" href="<?php echo url('hot') ?>">
        <i class="fab fa-hotjar"></i>
        <span>What's Hot</span></a>
    </li>
    <hr>
    <li class="nav-item">
      <a class="nav-link" href="<?php echo url('') ?>">
        <i class="far fa-arrow-alt-circle-left"></i>
        <span>Back to main menu</span></a>
    </li>
  </ul>

  <div id="content-wrapper">

    <div class="container-fluid">

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="admin">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">Promotions</li>
    </ol>

    <!-- table -->
    <div class="card mb-3">
      <div class="card-header">
        <button type="button" class="btn btn-secondary float-right" data-toggle="modal" data-target="#tambahUserModal">Tambah Promo</button>
        <i class="fas fa-table"></i>
        Data Menu</div>
      <div class="row">
        <div class="card-body col-10">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
              <tbody>
                <?php foreach ($hots as $hot):
                foreach ($menus as $menu):
                $idHot = $hot['id_hot'];
                $image = $hot['image'];
                $idProduct = $hot['id_product'];
                if ($menu['id_product'] == $hot['id_product']) {
                    ?>
                <tr>
                  <td><?php echo $menu['name'] ?></td>
                  <td><img style="width:300px" src="<?php echo asset($image) ?>"></td>
                  <td><button type="button" class="btn btn-danger fas fa-trash-alt hot-delete" data-toggle="modal" data-target="#buttonDelete" data-id="<?php echo $idHot ?>"></button>
                    <button type="button" class="btn btn-info fas fa-edit hot-update"
                     data-toggle="modal"
                      data-target="#updateMenuModal"
                       data-id="<?php echo $idHot ?>"
                       data-idproduct="<?php echo $idProduct ?>"
                       data-product="<?php echo $menu['name'] ?>"
                       data-img="<?php echo asset($image) ?>">
                  </td>
                </tr>
              <?php
                } endforeach;
                endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>

      <!-- Tambah Modal-->
      <div class="modal fade" id="tambahUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Promotion</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="post" action="<?php echo url('admin/hot/insert') ?>" id="insert-food-form" enctype="multipart/form-data">
            <div class="form-group">
            <div class="form-group">
              <label for="inputEmail1">Masukkan Gambar:</label>
              <input name="image" type="file" class="form-control" id="inputEmail1" aria-describedby="emailHelp" required>
            </div>
            <div class="form-group">
              <label for="formControlSelect1">Products</label>
              <select name="id_product" class="form-control" id="formControlSelectId">
                <?php foreach ($menus as $menu):?>
                <option value="<?php echo $menu["id_product"] ?>"><?php echo $menu["name"] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-primary" type="submit" form="insert-food-form">Tambah</button>
        </div>
      </div>
      </div>
      </div>
      </div>

      <!-- update -->
      <div class="modal fade" id="updateMenuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Promo</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php echo url('admin/hot/update') ?>" method="post" id="insert-update" enctype="multipart/form-data">
            <div class="form-group">
            <div class="form-group">
              <label for="inputEmail1">Masukkan Gambar:</label>
              <div class="">
              <img id="inputImage" src="" style="width:50%">
            </div>
              <input name="image" accept="image/*" type="file" >
            </div>
            <div class="form-group">
              <label for="formControlSelect1">Products</label>
              <select class="form-control" id="product-menu-update" name="id_product_update">
                <?php foreach ($menus as $menu):?>
                  <option value="<?php echo $menu["id_product"] ?>"><?php echo $menu["name"] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <input id="hot-id-update" type="hidden" name="id_hot" value="">
          </div>
          </form>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-primary" type="submit" form="insert-update">Update</button>
        </div>
      </div>
      </div>
      </div>
      </div>

      <!-- hapus -->
      <div class="modal fade" id="buttonDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Promo?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <form class="" action="<?php echo url('admin/hot/delete') ?>" method="post">
            <input id="hot-id" type="hidden" name="id_hot" value="">
          <button class="btn btn-primary" type="submit">Hapus</button>
        </form>
        </div>
      </div>
      </div>
      </div>
</div>
<?php partial('admin/footer', ['js' => ['admin/hot']]) ?>

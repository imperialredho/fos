<?php
allowGuestOnly();
partial('head', ['title' => 'Login']) ?>
	<div class="container login-background">
		<?php if (flashExists('error')): ?>
			<div class="alert alert-danger">
				<?php echo flash('error') ?>
			</div>
		<?php endif ?>
		<div class="row">
			<div class="col col-offset-1 content">
				<h1>Login</h1>
				<form method="post" action="<?php echo url('login') ?>">
						<div class="form-group">
							<label>Email</label><br>
							<input class="form-control" type="email" name="email" required><br>
						</div>
						<div class="form-group ">
							<label>Password</label><br>
							<input class="form-control" type="password" name="password" required><br>
						</div>
						<button type="submit" class="btn btn-warning">Login</button>
				</form>
			</div>
		</div>
	</div>
<?php partial('tail') ?>

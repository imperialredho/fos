<?php partial('head', ['title' => 'Address Selection']) ?>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
   integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
   crossorigin=""/>
   <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.2.13/dist/esri-leaflet-geocoder.css"
    integrity="sha512-v5YmWLm8KqAAmg5808pETiccEohtt8rPVMGQ1jA6jqkWVydV5Cuz3nJ9fQ7ittSxvuqsvI9RSGfVoKPaAJZ/AQ=="
    crossorigin="">
    <link  href="https://unpkg.com/leaflet-geosearch@latest/assets/css/leaflet.css" rel="stylesheet" />

    <!-- Content -->
    <div class="container mb-4">
      <h4 class="mt-0 mb-2">Kirim Ke</h4>

      <div class="row">
        <div class="col-12 col-md-6">
          <p class="text-white map-hint">
            <small class="d-md-none">Klik dua kali pada Map untuk lokasi pengiriman</small>
            <span class="d-none d-md-inline">Klik dua kali pada Map untuk lokasi pengiriman</span>
          </p>
          <div id="map" class></div>
        </div>

        <div class="col-12 col-md-6 text-center mb-md-3">
            <form method="post" action="<?php echo url('addresselect') ?>">
                <h5>Alamat yang anda pilih dari Map:</h5>
                <p class="address text-white">-- No address --</p>

                <h4>ATAU</h4>

                <p class="text-white">Masukkan lokasi pengiriman secara manual, lalu klik 'Enter'</p>
                <div class="form-group">
                    <textarea class="form-control addressManualInput" name="addressManualInput" placeholder="Alamat, Kecamatan, Kelurahan"></textarea>
                </div>

                <button class="btn btn-warning btn-block btn-order" type="submit">Lanjutkan Pemesanan</button>
                <input class="form-control addressMapInput" type="hidden" name="addressMapInput" value="">
            </form>
        </div>
      </div>
    </div>

<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
   integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
   crossorigin=""></script>
<script src="https://unpkg.com/esri-leaflet@2.2.3/dist/esri-leaflet.js"
    integrity="sha512-YZ6b5bXRVwipfqul5krehD9qlbJzc6KOGXYsDjU9HHXW2gK57xmWl2gU6nAegiErAqFXhygKIsWPKbjLPXVb2g=="
    crossorigin=""></script>
<script src="https://unpkg.com/esri-leaflet-geocoder@2.2.13/dist/esri-leaflet-geocoder.js" integrity="sha512-zdT4Pc2tIrc6uoYly2Wp8jh6EPEWaveqqD3sT0lf5yei19BC1WulGuh5CesB0ldBKZieKGD7Qyf/G0jdSe016A==" crossorigin=""></script>
<script src="https://unpkg.com/leaflet-geosearch@latest/dist/bundle.min.js"></script>
<script src="assets/js/map.js"></script>
<?php partial('tail') ?>

<!DOCTYPE html>
<html>
<head>
    <title><?php echo isset($title) ? $title . ' |' : '' ?> Nasi Goreng Padang Pak Arul</title>
    <meta name="root-url" content="<?php echo url('') ?>">
    <link rel="stylesheet" href="<?php echo asset('css/bootstrap.min.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo asset('css/style.css') ?>">
    <link rel="stylesheet" type="text/css" media="only screen and (min-width: 576px)" href="<?php echo asset('css/style-md.css') ?>">
    <link rel="stylesheet" type="text/css" media="only screen and (min-width: 768px)" href="<?php echo asset('css/style-lg.css') ?>">
    <link rel="stylesheet" type="text/css" media="only screen and (min-width: 992px)" href="<?php echo asset('css/style-xl.css') ?>">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body style="padding-top:150px">
    <?php partial('nav') ?>
    <main id="app">

<div class="fixed-top">
	<div class="navbar navbar-expand-sm bg-warning navbar-warning">
		<div class="container justify-content-center justify-content-sm-end">
			<div class="auth-info text-center text-sm-left p-1" style="">
				<?php if (isAuth()): ?>
					Halo, Anda Login sebagai <?php echo getAuthUser()['name'] ?>
				<?php else: ?>
					<span class="d-none d-sm-inline">Halo, selamat datang di</span> Nasi Goreng Padang Arul
	      <?php endif ?>
			</div>
      <?php if (isAuth()): ?>
				<a class="btn btn-warning" href="<?php echo url('cart') ?>">
          <i class="fas fa-shopping-cart"></i>
          <span class="badge cart-count">
						<?php echo getAuthUser()['cart_count'] ?? 0 ?>
					</span>
        </a>
				<?php if (isPrivilege(2)): ?>
					<a class="btn btn-warning" href="<?php echo url('profile') ?>">Akun Saya</a>
				<?php elseif (isPrivilege(1)): ?>
					<a class="btn btn-warning" href="<?php echo url('admin') ?>">Dasbor</a>
				<?php endif; ?>
				<a class="btn btn-warning" href="<?php echo url('logout') ?>">Log Out</a>
			<?php else: ?>
        <div>
          <a class="btn btn-warning" data-toggle="modal" data-target="#login-modal">Log In</a>
        </div>
        <div>
          <a class="btn btn-warning" href="<?php echo url('signup') ?>">Daftar</a>
        </div>
			<?php endif; ?>
		</div>
	</div>
	<div class="navbar navbar-expand-sm bg-dark navbar-dark">
		<div class="container">
			<a class="navbar-brand" href="<?php echo url('') ?>">
        <img class="logo" src="<?php echo asset('img/logop.png') ?>">
    	</a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
	      <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse justify-content-end" id="main-nav">
	      <ul class="navbar-nav">
					<li class="nav-item">
            <a class="nav-link" href="<?php echo url('') ?>">Beranda</a>
          </li>
					<li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="nav-menu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Menu
            </a>
            <div class="dropdown-menu" aria-labelledby="nav-menu">
              <a class="dropdown-item" href="<?php echo url('foods') ?>">Makanan</a>
              <a class="dropdown-item" href="<?php echo url('drinks') ?>">Minuman</a>
            </div>
          </li>
					<li class="nav-item">
            <a class="nav-link" href="<?php echo url('tentangkami') ?>">Tentang</a>
          </li>
					
	      </ul>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="login-modal-center" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content bg-dark">
      <div class="modal-header border-0">
        <h5 class="modal-title m-0">Log in</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo url('login') ?>" id="login-form">
					<div class="form-group">
						<label>Email</label>
						<input class="form-control" type="email" name="email" required>
					</div>
					<div class="form-group">
						<label>Password</label>
						<input class="form-control" type="password" name="password" required>
					</div>
				</form>
      </div>
			<div class="modal-footer border-0">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<button type="submit" class="btn btn-warning" form="login-form">Login</button>
			</div>
    </div>
  </div>
</div>

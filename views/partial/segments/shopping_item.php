<div class="media shopping-item shopping-background">
    <img class="mr-3 cart-img" src="<?php echo isset($product['photo']) ? asset($product['photo']) : 'https://via.placeholder.com/64' ?>" alt="<?php echo $product['name'] ?>">
    <div class="media-body">
        <div class="row">
            <div class="col-sm-6">
                <h5 class="mt-0 mb-1"><?php echo $product['name'] ?></h5>
                <p class="d-none d-md-block"><?php echo $product['description'] ?></p>
                <p class="m-0 mb-2 font-weight-bold">
                  Rp<?php echo number_format($product['price'], 0, ',', '.') ?>
                  (&times; <span class="jumlah-text"><?php echo $grocery['quantity'] ?></span>)
                </p>
            </div>
            <div class="col-sm-6 d-flex align-items-center justify-content-sm-end">
                <button type="button" class="btn btn-warning jumlah-up" data-id="<?php echo $grocery['id_product'] ?>"><i class="fas fa-chevron-up"></i></button>
                <span class="jumlah-text mx-2"><?php echo $grocery['quantity'] ?></span>
                <button type="button" class="btn btn-warning jumlah-down" data-id="<?php echo $grocery['id_product'] ?>"><i class="fas fa-chevron-down"></i></button>
                <button type="button" class="btn btn-warning delete ml-2" data-id="<?php echo $grocery['id_product'] ?>"><i class="far fa-times-circle"></i></button>
            </div>
        </div>
    </div>
</div>

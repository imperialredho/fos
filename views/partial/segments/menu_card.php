<!-- MEDIUM CARD -->
<div class="card-menu bg-dark text-white mb-4 d-none d-sm-block">
  <img class="card-img-top" src="<?php echo isset($product['photo']) ? asset($product['photo']) : 'https://via.placeholder.com/256' ?>" alt="<?php echo $product['name'] ?>">
  <div class="card-header text-center text-sm-left">
    <h5 class="p-0 m-0"><?php echo $product['name'] ?></h5>
  </div>
  <div class="card-body"><?php echo $product['description'] ?></div>
  <div class="card-footer clearfix">
    <?php if (isAuth()): ?>
    <button type="button" class="btn btn-warning float-right item-choose" data-id="<?php echo $product['id_product'] ?>" data-name="<?php echo $product['name'] ?>">Pilih</button>
    <?php else: ?>
    <button type="button" class="btn btn-warning float-right" data-toggle="modal" data-target="#login-modal">Pilih</button>
    <?php endif ?>
    <p class="font-weight-bold">Rp<?php echo number_format($product['price'], 0, ',', '.') ?></p>
  </div>
</div>

<!-- SMALL CARD -->
<div class="card-menu bg-dark text-white d-sm-none mb-3">
  <div class="card-body">
    <div class="media">
      <img class="menu-image-sm mr-3" src="<?php echo isset($product['photo']) ? asset($product['photo']) : 'https://via.placeholder.com/128' ?>" alt="<?php echo $product['name'] ?>">
      <div class="media-body">
        <h5 class="mt-0 mb-1"><?php echo $product['name'] ?></h5>
        <div class="my-2"><?php echo $product['description'] ?></div>
        <p class="font-weight-bold mb-0">Rp<?php echo number_format($product['price'], 0, ',', '.') ?></p>
      </div>
    </div>
  </div>
  <div class="card-footer">
    <?php if (isAuth()): ?>
    <button type="button" class="btn btn-warning btn-block item-choose" data-id="<?php echo $product['id_product'] ?>" data-name="<?php echo $product['name'] ?>">Pilih</button>
    <?php else: ?>
    <button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#login-modal">Pilih</button>
    <?php endif ?>
  </div>
</div>

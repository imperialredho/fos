    </main>
<?php partial('footer') ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?php echo asset('js/bootstrap.min.js') ?>"></script>

<?php if (isset($js)): ?>
<?php foreach ($js as $script): ?>
    <script src="<?php echo asset('js/' . $script . '.js') ?>"></script>
<?php endforeach ?>
<?php endif ?>

</body>
</html>
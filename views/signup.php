<?php
allowGuestOnly();
 partial('head', ['title' => 'Daftar'])?>
		<?php if (flashExists('error')): ?>
			<div class="alert alert-danger">
			<?php echo flash('error') ?>
			</div>
		<?php endif ?>
	<div class="container login-background">
		<div class="row">
			<div class="col-1"></div>
			<div class="col content">
				<h1>Daftar</h1>
				<form method="post" action="<?php echo url('signup') ?>">
					<div class="form-group">
						<label>Nama*</label><br>
						<input class="form-control" type="text" name="nama" required><br>
					</div>
					<div class="form-group">
						<label>Email*</label><br>
						<input class="form-control" type="email" name="email" required><br>
					</div>
					<div class="form-group ">
						<label>No. Telepon*</label><br>
						<input class="form-control" type="text" name="telepon" required><br>
					</div>
					<div class="form-group ">
						<label>Password*</label><br>
						<input class="form-control" type="password" name="password" required><br>
					</div>
					<div class="form-group ">
						<label>Re-Confirm Password*</label><br>
						<input class="form-control" type="password" name="passwordconfirm" required><br>
					</div>
					<p>*Wajib diisi</p>
					<button type="submit" class="btn btn-warning ">Sign Up</button>

				</form>
			</div>
			<div class="col-1"></div>
		</div>
	</div>
<?php partial('tail') ?>

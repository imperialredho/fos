<?php partial('head', ['title' => 'Shopping Cart']) ?>
	<div class="container">
        <h4 class="mt-0 mb-2">Keranjang Saya</h4>
    </div>
	<div class="shopping-list">
		<?php foreach ($groceries as $grocery): ?>
			<?php $product = findProductById($products, $grocery['id_product']); ?>

			<div class="d-none d-sm-block container">
                <?php partial('segments/shopping_item', compact('product', 'grocery')) ?>
            </div>
            <div class="d-sm-none">
                <?php partial('segments/shopping_item', compact('product', 'grocery')) ?>
            </div>

		<?php endforeach; ?>
	</div>
    <div class="container my-3 my-md-4 text-md-right">
    	<a class="btn btn-warning btn-order btn-lg d-none d-md-inline-block" href="<?php echo url('addresselect') ?>">Lanjut ke Pemesanan</a>
    	<a class="btn btn-warning btn-block btn-order d-md-none" href="<?php echo url('addresselect') ?>">Lanjut ke Pemesanan</a>
    </div>
<?php partial('tail', ['js' => ['cart']]) ?>

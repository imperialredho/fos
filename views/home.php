<?php partial('head') ?>
	<!-- Content -->
	<div class="container">
		<div class="row">
			<div class="col content">
				<div id="featured" class="carousel slide" data-ride="carousel">
					<ul class="carousel-indicators">
					<?php
						$data = 0;
						foreach ($hots as $hot):
					?>
						<li data-target="#featured" data-slide-to="<?php echo($data)?>" <?php if ($data==0): ?>
							class="active"<?php endif ?>></li>
					<?php
						$data = $data+1;
						endforeach;
					?>
					</ul>
					<div class="carousel-inner">
						<?php
							$data = 0;
							foreach ($hots as $hot):
						?>
						<div <?php if ($data==0): ?>
								class="carousel-item active"
							<?php else:?>
								class="carousel-item"
							<?php endif;?>>
							<a class="hot" href="javascript:;" data-id="<?php echo $hot['id_product'] ?>">
								<img class="img-fluid" src="<?php echo asset($hot['image']) ?>" alt="What's Hot">
							</a>
						</div>
						<?php $data = $data+1;
							endforeach;?>
					</div>
					<a class="carousel-control-prev" href="#featured" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					</a>
					<a class="carousel-control-next" href="#featured" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
					</a>
				</div>
			</div>
		</div>
		<div class="row my-sm-4">
			<div class="col-12 col-sm-4">
				<a href="<?php echo url('foods') ?>" class="d-block pb-3 text-center">
					<img class="img-fluid quick-box d-inline-block d-sm-none" src="assets/img/mak-xs.png">
					<img class="img-fluid quick-box d-none d-sm-inline-block d-md-none" src="assets/img/mak-sm.png">
					<img class="img-fluid quick-box d-none d-md-inline-block d-lg-none" src="assets/img/mak-md.png">
					<img class="img-fluid quick-box d-none d-lg-inline-block d-xl-none" src="assets/img/mak-lg.png">
					<img class="img-fluid quick-box d-none d-xl-inline-block" src="assets/img/mak-xl.png">
				</a>
			</div>
			<div class="col-12 col-sm-4">
				<a href="<?php echo url('drinks') ?>" class="d-block pb-3 text-center">
					<img class="img-fluid quick-box d-inline-block d-sm-none" src="assets/img/min-xs.png">
					<img class="img-fluid quick-box d-none d-sm-inline-block d-md-none" src="assets/img/min-sm.png">
					<img class="img-fluid quick-box d-none d-md-inline-block d-lg-none" src="assets/img/min-md.png">
					<img class="img-fluid quick-box d-none d-lg-inline-block d-xl-none" src="assets/img/min-lg.png">
					<img class="img-fluid quick-box d-none d-xl-inline-block" src="assets/img/min-xl.png">
				</a>
			</div>
			<div class="col-12 col-sm-4">
				<a href="<?php echo url('tentangkami') ?>" class="d-block pb-3 text-center">
					<img class="img-fluid quick-box d-inline-block d-sm-none" src="assets/img/tk-xs.png">
					<img class="img-fluid quick-box d-none d-sm-inline-block d-md-none" src="assets/img/tk-sm.png">
					<img class="img-fluid quick-box d-none d-md-inline-block d-lg-none" src="assets/img/tk-md.png">
					<img class="img-fluid quick-box d-none d-lg-inline-block d-xl-none" src="assets/img/tk-lg.png">
					<img class="img-fluid quick-box d-none d-xl-inline-block" src="assets/img/tk-xl.png">
				</a>
			</div>
		</div>
	</div>

	<?php if (isAuth()): ?>
		<div class="modal fade" tabindex="-1" role="dialog" id="product-ordered">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title text-dark"><span class="product-name"></span> telah ditambahkan ke keranjang!</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body text-dark">
		        <p>Lanjut memesan atau lihat keranjang?</p>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Lanjut Memesan</button>
		        <a class="btn btn-warning" href="<?php echo url('cart') ?>">Lihat Keranjang</a>
		      </div>
		    </div>
		  </div>
		</div>
	<?php endif ?>
<?php partial('tail' , ['js' => ['home']]) ?>

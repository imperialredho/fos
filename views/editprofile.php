<?php partial('head', ['title' => 'Edit Akun']) ?>
	<div class="container login-background">
		<div class="row">
			<?php if (flashExists('error')): ?>
			<div class="alert alert-danger">
				<?php echo flash('error') ?>
			</div>
		<?php endif ?>
			<div class="col-1"></div>
			<div class="col content">
				<h1>Edit Akun</h1>
				<form method="post" action="<?php echo url('editprofile') ?>">
					<div class="form-group">
						<label>Nama*</label><br>
						<input class="form-control" type="text" name="name" value="<?php echo getAuthUser()['name'] ?>" required><br>
					</div>
					<div class="form-group ">
						<label>No. Telepon*</label><br>
						<input class="form-control" type="text" name="phone" value="<?php echo getAuthUser()['phone'] ?>" required><br>
					</div>
					<div class="form-group ">
						<label>Password Lama*</label><br>
						<input class="form-control" type="password" name="password"><br>
					</div>
					<div class="form-group ">
						<label>Password Baru*</label><br>
						<input class="form-control" type="password" name="newpassword"><br>
					</div>
					<div class="form-group ">
						<label>Re-Confirm Password Baru*</label><br>
						<input class="form-control" type="password" name="newpasswordconfirm"><br>
					</div>
					<p>*Wajib diisi</p>
					<button type="submit" class="btn btn-warning ">Update</button>

				</form>
			</div>
			<div class="col-1"></div>
		</div>
	</div>
<?php partial('tail') ?>
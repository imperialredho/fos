<?php partial('head', ['title' => 'Tidak Ditemukan']) ?>
	<div class="container text-white text-center">
        <h1>:(</h1>
        <h3>Halaman tidak ditemukan</h3>
        <p>Silahkan kembali ke halaman sebelumnya.</p>
    </div>
<?php partial('tail') ?>
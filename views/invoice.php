
<!------ Include the above in your HEAD tag ---------->

<?php partial('head', ['title' => 'Bukti Pemesanan']) ?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card text-white bg-dark">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-3">
                            <img class="img-fluid" src="<?php echo asset('img/logop.png') ?>">
                        </div>
                        <div class="col-12 col-sm text-center text-sm-right py-3 py-sm-0">
                            <p class="font-weight-bold m-0 small">Invoice Order #<?php echo $order['id_order'] ?></p>
                            <p class="text-muted m-0"><?php echo date('l, j F Y H:i:s', strtotime($order['created_at'])) ?></p>
                        </div>
                    </div>

                    <hr class="mb-3">
                    
                    <div class="text-center ">
                        <p class="font-weight-bold m-0 small">Informasi Pembelian</p>
                        <p class="m-0">Bapak/Ibu <?php echo getAuthUser()['name'] ?></p>
                        <p class="m-0"><?php echo $order['address'] ?></p>
                    </div>

                    <hr class="mb-3">

                    <table class="table">
                        <thead>
                            <tr>
                                <th class="border-0 text-uppercase small font-weight-bold">Nama</th>
                                <th class="border-0 text-uppercase small font-weight-bold">Harga</th>
                                <th class="border-0 text-uppercase small font-weight-bold">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($orderDetails as $detail): 
                                    $product = findProductById($products, $detail['id_product']);
                            ?>
                                <tr>
                                    <td class="small"><?php echo $product['name'] ?></td>
                                    <td class="small"><?php echo $product['price'] . ' &times; ' . $detail['quantity'] ?></td>
                                    <td class="small"><?php echo 'Rp '.($product['price']*$detail['quantity']) ?></td>
                                </tr>
                            <?php
                                endforeach; ?>
                            <tr>
                                <td class="small"></td>
                                <td class="small">Delivery Fee</td>
                                <td class="small"><?php echo 'Rp '.$order['delivery_fee'] ?></td>
                            </tr>
                            <tr>
                                <td class="small"></td>
                                <td class="small">Total</td>
                                <td class="small"><?php echo 'Rp '.$order['total_fee'] ?></td>
                            </tr>
                        </tbody>
                    </table>

                    <hr class="mb-3">

                    <p class="lead text-center">~ Terima kasih telah memesan di Nasi Goreng Padang Pak Arul ~</p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php partial('tail') ?>


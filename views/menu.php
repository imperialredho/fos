<?php partial('head', ['title' => $title]) ?>

	<div class="container">
		<h4 class="mt-0 mb-2"><?php echo $title ?></h4>
		<p class="text-white mb-4">Klik pada tombol "Pilih" untuk memesan.</p>
		<ul class="nav nav-tabs nav-fill nav-menu my-3">
			<li class="nav-item">
				<a class="nav-link
					<?php if ($title === 'Menu: Makanan'): echo 'active'; endif ?>"
					href="<?php echo url('foods') ?>">Makanan</a>
			</li>
			<li class="nav-item">
				<a class="nav-link
					<?php if ($title === 'Menu: Minuman'): echo 'active'; endif ?>"
					href="<?php echo url('drinks') ?>">Minuman</a>
			</li>
		</ul>
		<div class="row">
			<?php foreach ($products as $product): ?>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<?php partial('segments/menu_card', ['product' => $product]); ?>
				</div>
			<?php endforeach; ?>
		</div>
	</div>

	<?php if (isAuth()): ?>
	<div class="modal fade" tabindex="-1" role="dialog" id="product-ordered">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title text-dark"><span class="product-name"></span> telah ditambahkan ke keranjang!</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body text-dark">
	        <p>Lanjut memesan atau lihat keranjang?</p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Lanjut Pesan</button>
	        <a class="btn btn-warning" href="<?php echo url('cart') ?>">Lihat Keranjang</a>
	      </div>
	    </div>
	  </div>
	</div>
	<?php endif ?>
<?php partial('tail', ['js' => ['menu']]) ?>

<?php partial('head', ['title' => 'Order Confirmation']) ?>
	<!-- Content -->
	<div class="container">
		<h4 class="mt-0 mb-2">Konfirmasi Pesanan</h4>
		<h5 class="subheader pb-1">Alamat Pengiriman</h5>

		<p class="text-white"><?php echo $_SESSION['address'] ?></p>
		<h5 class="subheader pb-1">Pesanan Anda</h5>
	</div>
	<?php
		$totalprice = 0;
		// echo("{$_SESSION['address']}");
		foreach ($groceries as $grocery): 
			$product = findProductById($products, $grocery['id_product']);
			$itemPrice = $product['price'] * $grocery['quantity'];
	?>
		<div class="d-none d-sm-block container">
			<div class="media shopping-item shopping-background">
			    <img class="mr-3 cart-img" src="<?php echo isset($product['photo']) ? asset($product['photo']) : 'https://via.placeholder.com/64' ?>" alt="<?php echo $product['name'] ?>">
			    <div class="media-body">
			        <h5 class="mt-0 mb-1"><?php echo $product['name'] ?></h5>
			        <p>Rp<?php echo number_format($product['price'], 0, ',', '.') ?> &times; <span class="jumlah-text"><?php echo $grocery['quantity'] ?></span>
			        	<span class="float-right">= Rp<?php echo number_format($itemPrice, 0, ',', '.') ?></span>
			        </p>
			    </div>
			</div>
		</div>
		<div class="d-sm-none">
			<div class="media shopping-item shopping-background">
			    <img class="mr-3 cart-img" src="<?php echo isset($product['photo']) ? asset($product['photo']) : 'https://via.placeholder.com/64' ?>" alt="<?php echo $product['name'] ?>">
			    <div class="media-body">
			        <h5 class="mt-0 mb-1"><?php echo $product['name'] ?></h5>
			        <p>Rp<?php echo number_format($product['price'], 0, ',', '.') ?> &times; <span class="jumlah-text"><?php echo $grocery['quantity'] ?></span>
			        	<span class="float-right">= Rp<?php echo number_format($itemPrice, 0, ',', '.') ?></span>
			        </p>
			    </div>
			</div>
		</div>
	<?php 
			$totalprice += $itemPrice;
		endforeach;
	?>
	<div class="d-none d-sm-block container">
		<div class="media shopping-item shopping-background">
		    <div class="media-body">
			    <p class="mt-0 mb-1">
			    	Delivery Fee
			    	<span class="float-right">Rp2.000</span>
			    </p>
			</div>
		</div>
		<div class="media shopping-item shopping-background" style="background-color:#ffc107aa;color:#000">
			<div class="media-body">
				<h5 class="mt-0 mb-1">
			    	Total
			    	<span class="float-right">Rp<?php echo number_format($totalprice + 2000, 0, ',', '.') ?></span>
			    </h5>
			</div>
		</div>
	</div>
	<div class="d-sm-none">
		<div class="media shopping-item shopping-background">
		    <div class="media-body">
			    <p class="mt-0 mb-1">
			    	Delivery Fee
			    	<span class="float-right">Rp2.000</span>
			    </p>
			</div>
		</div>
		<div class="media shopping-item shopping-background" style="background-color:#ffc107aa;color:#000">
			<div class="media-body">
				<h5 class="mt-0 mb-1">
			    	Total
			    	<span class="float-right">Rp<?php echo number_format($totalprice + 2000, 0, ',', '.') ?></span>
			    </h5>
			</div>
		</div>
	</div>
	<div class="container mt-2 mb-3">
		<form method="post" action="<?php echo url('makeorder') ?>">
			<input type="hidden" name="totalprice" value="<?php echo $totalprice ?>">
			<div class="form-group">
				<label>Catatan Tambahan</label>
				<textarea class="form-control" type="text" name="note"></textarea>
			</div>
			<button class="btn btn-warning btn-block" type="submit">Pesan Sekarang</button>
		</form>
	</div>
<?php partial('tail') ?>
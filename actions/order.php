<?php
include_once 'database/select.php';
allowPrivilegedOnly(1, '');

$orders = select('orders', '*', [], [], 100);

view('admin/order', compact('orders'));

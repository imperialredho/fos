<?php
	if(isset($_POST['addressMapInput']) || isset($_POST['addressManualInput'])) {
		
		if ($_POST['addressMapInput']=="") {

			if (empty($_POST['addressManualInput'])) {
				$_SESSION['address'] = "";
				redirect('addressselect');
			} else {
				$_SESSION['address'] = $_POST['addressManualInput'];
				redirect('orderconfirmation');
			}
		} else {
			$_SESSION['address'] = $_POST['addressMapInput'];
			redirect('orderconfirmation');	
		}

	}

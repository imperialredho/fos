<?php

include_once 'database/select.php';

$products = select('products', '*', [
    ['and', 'type', '=', '2']
]);

$title = 'Menu: Minuman';

view('menu', compact('products', 'title'));

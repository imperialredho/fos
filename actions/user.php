<?php
include_once 'database/select.php';
allowPrivilegedOnly(1, '');

$users = select('users', '*', [], [], 100);

view('admin/users', compact('users'));

<?php

include_once 'database/select.php';
include_once 'database/delete.php';

$email = getAuthUser()['email'];
$conditions = [
	['and', 'email', '=', $email],
	['and', 'id_product', '=', $inputs['id_product']],
];

delete('groceries', $conditions);

echo json_encode(['success' => true]);
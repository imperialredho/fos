<?php

include_once 'database/select.php';
include_once 'database/insert.php';
include_once 'database/update.php';

$email = getAuthUser()['email'];
$conditions = [
	['and', 'email', '=', $email],
	['and', 'id_product', '=', $inputs['id_product']],
];

$grocery = selectOne('groceries', '*', $conditions);

if (is_null($grocery)) {
	insert('groceries', [
		'email' => $email,
		'id_product' => $inputs['id_product'],
		'quantity' => 1,
	]);
} else {
	update('groceries', ['quantity' => $grocery['quantity'] + 1], $conditions);
}

echo json_encode(['success' => true]);

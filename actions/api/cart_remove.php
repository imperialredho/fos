<?php

include_once 'database/select.php';
include_once 'database/delete.php';
include_once 'database/update.php';

$email = getAuthUser()['email'];
$conditions = [
	['and', 'email', '=', $email],
	['and', 'id_product', '=', $inputs['id_product']],
];

$grocery = selectOne('groceries', '*', $conditions);

if ($grocery['quantity'] > 1) {
	update('groceries', ['quantity' => $grocery['quantity'] - 1], $conditions);
} else {
	delete('groceries', $conditions);
}

echo json_encode(['success' => true]);
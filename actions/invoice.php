<?php

include_once 'database/select.php';
include_once 'database/products.php';

$email = getAuthUser()['email'];

$order = selectOne('orders', '*', [
	['and', 'email', '=', $email]],
	['created_at' => 'DESC']
);

$orderDetails = select('order_details', '*', [
	['and', 'id_order', '=', $order['id_order']]
]);

$products = select('products', '*');

view('invoice', compact('order', 'orderDetails', 'products'));
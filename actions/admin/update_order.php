<?php

include_once 'database/update.php';

update('orders', [
  'address'=> $inputs['update_address'],
  'delivery_fee' => $inputs['update_fee'],
  'total_fee' => $inputs['update_fee2'],
  'note' => $inputs['update_note'],
], [['and', 'id_order', '=', $inputs['id_order']]]);

redirect('order');

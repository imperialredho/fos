<?php

include_once 'database/delete.php';

delete('order_details', [
  ['and', 'id_order', '=', $inputs['id_order']],
]);

delete('orders', [
  ['and', 'id_order', '=', $inputs['id_order']],
]);

redirect('order');

<?php

include_once 'database/insert.php';
include_once 'utilities/file.php';

$photo = upload('img/', 'photo');

insert('products', [
  'name' => $inputs['name'],
  'description' => $inputs['desc'],
  'type' => $inputs['type'],
  'price' => $inputs['price'],
  'photo' => $photo,
]);

redirect('food');

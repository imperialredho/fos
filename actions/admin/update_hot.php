<?php

include_once 'database/update.php';
include_once 'utilities/file.php';

$updates = [
  'id_product' => $inputs['id_product_update'],
];

if (isUploaded('image')) {
  $updates['image'] = upload('img/', 'image');
}

update('promotions', $updates, [['and', 'id_hot', '=', $inputs['id_hot']]]);

redirect('hot');

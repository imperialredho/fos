<?php

include_once 'database/insert.php';

insert('users', [
  'name' => $inputs['name'],
  'email' => $inputs['email'],
  'password' => $inputs['password'],
  'phone' => $inputs['phone'],
  'privilege' => $inputs['privilege'],
]);

redirect('user');

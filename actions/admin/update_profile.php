<?php

include_once 'database/update.php';
include_once 'database/users.php';

$newpass = $inputs['newpassword'];
$newpasswordconfirm = $inputs['newpasswordconfirm'];
$pass = getAuthUser()['password'];
$password = $inputs['password'];

if ($pass == $password) {
  if ($newpass == $newpasswordconfirm) {
    update('users', [
      'name' => $inputs['username'],
      'password' => $newpass,
      'phone' => $inputs['phone'],
    ], [['and', 'email', '=', $inputs['email']]]);
    flash('success', 'Berhasil update');
    login(getUserByEmail(getAuthUser()['email']));
    redirect('adminprofile');
  }
} elseif ($pass !== $newpass) {
  flash('error', 'Password yang anda masukkan salah');
  redirect('adminprofile');
} elseif ($newpass !== $newpasswordconfirm) {
  flash('error', 'Password baru yang anda masukkan salah');
  redirect('adminprofile');
}else {
  flash('error', 'Pengisian password yang anda masukkan salah');
  redirect('adminprofile');
}

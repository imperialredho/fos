<?php

include_once 'database/insert.php';
include_once 'utilities/file.php';

$photo = upload('img/', 'image');

insert('promotions', [
  'image' => $photo,
  'id_product' => $inputs['id_product']
]);

redirect('hot');

<?php

include_once 'database/update.php';
include_once 'utilities/file.php';

$updates = [
  'name' => $inputs['update_name'],
  'description' => $inputs['update_desc'],
  'type' => $inputs['update_type'],
  'price' => $inputs['update_price'],
];

if (isUploaded('photo')) {
  $updates['photo'] = upload('img/', 'photo');
}

update('products', $updates, [['and', 'id_product', '=', $inputs['id_product2']]]);

redirect('food');

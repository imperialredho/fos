<?php
include_once 'database/select.php';

$foods = select('products', '*', [
    ['and', 'type', '=', '1']
]);

$drinks = select('products', '*', [
    ['and', 'type', '=', '2']
]);

$hots =  select('promotions', '*');

view('home', compact('foods', 'drinks', 'hots'));
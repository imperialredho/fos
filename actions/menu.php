<?php

include_once 'database/select.php';


if ($_POST['product']=="food") {
	$products = select('products', '*', [
    	['and', 'type', '=', '1']
	]);
} else {
	$products = select('products', '*', [
    	['and', 'type', '=', '2']
	]);
}

$type = $_POST['product'];

view('submenu', compact('products', 'type'));

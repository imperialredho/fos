<?php 

	include_once 'database/update.php';
	include_once 'database/users.php';

	$old_data = [
		'name' => $inputs['name'],
		'phone' => $inputs['phone'],
	];

	if ($inputs['newpassword'] !== $inputs['newpasswordconfirm']) {
	flash('error', 'Konfirmasi password baru tidak sesuai dengan password baru anda');
	redirect('editprofile');
	}

	if (!isset($inputs['password']) && isset($inputs['newpassword'])) {
	flash('error', 'Anda belum memasukkan password lama anda');
	redirect('editprofile');	
	}

	if (isset($inputs['password']) && $inputs['password'] !== getAuthUser()['password']) {
	flash('error', 'Password lama yang anda masukkan salah');
	redirect('editprofile');
	}

	if (isset($inputs['newpassword'])) {
		$old_data['password'] = $inputs['newpassword'];
	}

	update('users',$old_data,[
		['and','email','=',getAuthUser()['email']]
	] );
	flash('success', 'Anda berhasil memperbarui info akun anda!');
	login(getUserByEmail(getAuthUser()['email']));
	redirect ('');
?>
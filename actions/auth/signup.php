<?php

include_once 'utilities/redirect.php';
include_once 'database/users.php';
include_once 'database/insert.php';

if ($inputs['password'] !== $inputs['passwordconfirm']) {
	flash('error', 'Konfirmasi password tidak sesuai dengan password anda');
	redirect('signup');
}
else {
	insert ('users',[
		'email'=>$inputs['email'],
		'name'=>$inputs['nama'],
		'phone'=>$inputs['telepon'],
		'password'=>$inputs['password'],
		'privilege'=> 2,
	]);
    login(getUserByEmail($inputs['email']));
 redirect ('');
}
?>

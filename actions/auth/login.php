<?php

include_once 'database/users.php';

$email = $inputs['email'];
$password = $inputs['password'];

if (isCredentialsValid($email, $password)) {
    login(getUserByEmail($email));
} else {
	flash('error', 'Kombinasi email dan password salah!');
	redirect('login');
}

if (isPrivilege(1))
  redirect('admin');
else
  redirect('');

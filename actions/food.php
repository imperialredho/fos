<?php
include_once 'database/select.php';
allowPrivilegedOnly(1, '');

$menus = select('products', '*', [], [], 100);

view('admin/food', compact('menus'));

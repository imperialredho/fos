<?php
include_once 'database/select.php';
allowAuthOnly();

$groceries = select('groceries', '*', [
    ['and', 'email', '=', getAuthUser()['email']]
]);

if (empty($groceries)) {
	
	redirect('cart');

} else view('addressselect');

<?php
include_once 'database/select.php';
allowPrivilegedOnly(1, '');

$hots = select('promotions', '*', [], [], 100);
$menus = select('products', '*', [], [], 100);

view('admin/hot', compact('hots', 'menus'));

<?php

include_once 'database/select.php';

$products = select('products', '*', [
    ['and', 'type', '=', '1']
]);

$title = 'Menu: Makanan';

view('menu', compact('products', 'title'));

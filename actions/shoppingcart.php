<?php

include_once 'database/select.php';
include_once 'database/products.php';
allowAuthOnly();


$groceries = select('groceries', '*', [
    ['and', 'email', '=', getAuthUser()['email']]
]);

$products = select('products', '*', [], [], 100);

view('shoppingcart', compact('groceries', 'products'));

<?php

include_once 'database/select.php';
include_once 'database/insert.php';
include_once 'database/delete.php';

$email = getAuthUser()['email'];
$delivery_fee = 2000;

insert ('orders', [
	'address' => $_SESSION['address'],
	'email' => $email,
	'delivery_fee' => $delivery_fee,
	'total_fee' => $_POST['totalprice']+$delivery_fee,
	'note' => $inputs['note'],

]);	

$id_order = selectOne('orders', '*', [
	['and', 'email', '=', $email]],
	['created_at' => 'DESC']
);

$groceries = select('groceries', '*', [
    ['and', 'email', '=', $email]
]);

foreach ($groceries as $grocery) {

	insert ('order_details', [
		'id_order' => $id_order['id_order'],
		'id_product' => $grocery['id_product'],
		'quantity' => $grocery['quantity'],

	]);

}

delete('groceries', [
	['and', 'email', '=', $email]
]);

unset($_SESSION['address']);

redirect('invoice');
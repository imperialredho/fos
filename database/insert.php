<?php

include_once 'database/connection.php';

/**
 * Execute an INSERT query.
 * 
 * @param  string  $table
 * @param  array   $values
 * @return void|bool
 * 
 * @example 
 * 
 *  insert('orders', ['location' => 'Jakarta', 'price' => 25000]) 
 * 
 *  The above call translates to SQL "INSERT INTO orders ('location', 'price') VALUES ('Jakarta', 25000)",
 *  which executes an insertion to "orders" table with the values passed on the second parameter 
 *  as an associative array. Should the execution succeed, the function returns true. Otherwise, void.
 */
function insert($table, $values) {
    $connection = connect();
    $query = _buildInsertionQueryStatement($connection, $table, $values);
    $result = $connection->query($query);

    if (! $result) {
        throwSqlError($connection);
    } else {
        return $values;
    }
}

/**
 * PRIVATE FUNCTION
 */
function _buildInsertionQueryStatement($connection, $table, $values) {
    return 'INSERT INTO ' . $table . ' (' 
        . _extractFields($values) . ') VALUES (' 
        . _extractValues($connection, $values) . ')';
}

/**
 * PRIVATE FUNCTION
 */
function _extractFields($values) {
    return implode(', ', array_keys($values));
}

/**
 * PRIVATE FUNCTION
 */
function _extractValues($connection, $values) {
    $escaped = array_map(function ($item) use ($connection) {
        return '"' . mysqli_real_escape_string($connection, $item) . '"'; 
    }, $values);

    return implode(', ', $escaped);
}

<?php

include_once 'database/connection.php';
include_once 'database/where_builder.php';

/**
 * Execute an UPDATE query.
 * 
 * @param  string  $table
 * @param  array   $values
 * @param  array   $where
 * @return void|bool
 * 
 * @example 
 * 
 *  update('foods', ['name' => 'Bulgogi Rice Bowl'], [['and', 'id', '=', 2]]) 
 * 
 *  The above call translates to SQL "UPDATE foods SET name = 'Bulgogi Rice Bowl' WHERE id = 2",
 *  which executes an update to row(s) in "foods" table with the values passed on the second parameter, that have
 *  an id of 2, represented as an array of conditions in the third parameter. Should the execution succeed, 
 *  the function returns true. Otherwise, void.
 * 
 *  Check database/where_builder.php for condition writing guide.
 */
function update($table, $values, $where) {
    $connection = connect();
    $query = _buildUpdationQueryStatement($connection, $table, $values, $where);
    $result = $connection->query($query);

    if (! $result) {
        throwSqlError($connection);
    } else {
        return true;
    }
}

/**
 * PRIVATE FUNCTION
 */
function _buildUpdationQueryStatement($connection, $table, $values, $where) {    
    return 'UPDATE ' . $table . 
        ' SET ' . _buildSetStatements($connection, $values) . 
        ' ' . buildWhereStatement($connection, $where);
}

/**
 * PRIVATE FUNCTION
 */
function _buildSetStatements($connection, $values) {
    $normalized = array_map(function ($field, $value) use ($connection) {
        return $field . ' = "' . mysqli_real_escape_string($connection, $value) . '"'; 
    }, array_keys($values), $values);

    return implode(', ', $normalized);
}
<?php

include_once 'database/select.php';

/**
 * Check whether the given credentials are valid.
 *
 * @param  string  $email
 * @param  string  $password
 * @return boolean
 */
function isCredentialsValid($email, $password) {
	$user = selectOne('users', '*', [
		['and', 'email', '=', $email],
		['and', 'password', '=', $password],
	]);

	return ! is_null($user);
}

/**
 * Return a user identified by the given email.
 *
 * @param  string  $email
 * @return array|null
 */
function getUserByEmail($email) {
	return selectOne('users', '*', [
		['and', 'email', '=', $email],
	]);
}
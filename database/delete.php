<?php

include_once 'database/connection.php';
include_once 'database/where_builder.php';

/**
 * Execute a DELETE query.
 * 
 * @param  string  $table
 * @param  array   $where
 * @return void|bool
 * 
 * @example 
 * 
 *  delete('users', [['and', 'id', '=', 1]]) 
 * 
 *  The above call translates to SQL "DELETE FROM users WHERE id = 1",
 *  which executes a deletion to every row in "users" table with "id" of 1.
 *  Should the execution succeed, the function returns true. Otherwise, void.
 * 
 *  Check where_builder.php for condition writing guide.
 */
function delete($table, $where) {
    $connection = connect();
    $query = _buildDeletionQueryStatement($connection, $table, $where);
    $result = $connection->query($query);

    if (! $result) {
        throwSqlError($connection);
    } else {
        return true;
    }
}

/**
 * PRIVATE FUNCTION
 */
function _buildDeletionQueryStatement($connection, $table, $where) {    
    return 'DELETE FROM ' . $table .
        ' ' . buildWhereStatement($connection, $where);
}

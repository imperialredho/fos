<?php

/**
 * Build a WHERE clause from the given condition array.
 * 
 * @param  mysqli        $connection
 * @param  string|array  $where
 * @return string
 * 
 * @example
 *  
 *  This function should not be called manually, but rather, via SELECT, UPDATE or DELETE $where parameter.
 *  The $where parameter is an array of condition array. Each condition array follows this convention:
 *  [
 *      0 => The boolean of the condition (either and & or)
 *      1 => The row to be checked or group of conditions
 *      2 => The operator
 *      3 => The value to compare
 *  ]
 * 
 *  For example, if you pass this array: [
 *      ['and', 'name', 'like', '%dis%'], 
 *      ['and', 'location', '=', 'Bandung'],
 *  ]
 *  this function will return "name LIKE %dis% AND location = 'Bandung'"
 * 
 *  Another example, [
 *      ['and', [
 *         ['and', 'price', '<', '80000'],
 *         ['and', 'success', '=', true]
 *      ]],
 *      ['or', 'year', '>', '2015']
 *  ]
 *  will be translated to "(price < 80000 AND success = 1) OR year > 2015"
 * 
 *  The boolean condition of the first condition array in either list or group will be omitted as seen in
 *  the examples above.
 * 
 *  Check database/select.php, database/update.php, database/delete.php to see how this function is used.
 */
function buildWhereStatement($connection, $where) {
    if (empty($where)) {
        return '';
    } else if (is_string($where)) {
        return 'WHERE ' . $where; 
    } else {
        return 'WHERE ' . _normalizeConditionsList($connection, $where);
    }
}

/**
 * PRIVATE FUNCTION
 */
function _normalizeConditionsList($connection, $conditions) {
    $normalized = array_map(function ($item) use ($connection) {
        if (is_array($item[1])) {
            return trim($item[0] . ' (' . _normalizeConditionsList($item[1]) . ')');
        } else {
            return trim(_normalizeSingleCondition($connection, $item));
        }
    }, _clearFirstBoolean($conditions));

    return implode(' ', $normalized);
}

/**
 * PRIVATE FUNCTION
 */
function _normalizeSingleCondition($connection, $condition) {
    $filtered = $condition;
    $lastIndex = count($filtered) - 1;
    $filtered[$lastIndex] = '"' . mysqli_real_escape_string($connection, $filtered[$lastIndex]) . '"';
    return implode(' ', $filtered);
}

/**
 * PRIVATE FUNCTION
 */
function _clearFirstBoolean($conditions) {
    $cloned = $conditions;
    $cloned[0][0] = '';
    return $cloned;
}
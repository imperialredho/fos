<?php

/**
 * Find a product with the given id, from the given products array.
 * 
 * @param  array    $products
 * @param  integer  $id
 * @return array|null
 */
function findProductById($products, $id) {
    $product = array_filter($products, function ($item) use ($id) {
        return $item['id_product'] == $id;
    });
    return reset($product);
}
<?php

include_once 'config.php';

/**
 * Initialize a connection to MySQL Database.
 *
 * @return mysql connection object
 */
function connect() {
    global $CONFIG;
    $connect = new mysqli(
        $CONFIG['db_host'],
        $CONFIG['db_username'],
        $CONFIG['db_password'],
        $CONFIG['db_name']
    );

    if ($connect->connect_error) {
        die('Connection failed: ' . $connect->connect_error);
    }

    return $connect;
}

/**
 * Handle errors that occur in database level.
 *
 * @param  $connection
 * @return void
 */
function throwSqlError($connection) {
    die(mysqli_error($connection));
}
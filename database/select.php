<?php

include_once 'config.php';
include_once 'database/connection.php';
include_once 'database/where_builder.php';

/**
 * Execute a SELECT query and return multiple rows.
 * 
 * @param  string        $from
 * @param  string|array  $fields = '*'
 * @param  array         $where = []
 * @param  array         $order = []
 * @param  integer|null  $limit = null
 * @param  integer|null  $limit = skip
 * @return array
 * 
 * @example 
 * 
 *  select('offices')
 * 
 *  The above call translates to SQL "SELECT * FROM offices LIMIT $_CONFIG['default_limit'], $_CONFIG['default_skip']"
 *  and returns an array of rows. Each row is represented as array in which cells are represented as key value pairs.
 *  The default limit and skip are always called if the last two parameters are not present. Both can be configured in 
 *  config.php.
 * 
 *  select('offices', 
 *      ['name', 'address', 'zipcode' => 'postal_code'], 
 *      [['and', 'name', 'LIKE', '%Karawang%']],
 *      ['name' => 'desc'],
 *      25,
 *      50)
 * 
 * The above call translates to SQL "SELECT name, address, zipcode AS postal_code FROM offices WHERE name LIKE 
 * '%Karawang%' ORDER BY name DESC LIMIT 25, 50" and returns an array of rows, representing the result based on the
 * query.
 * 
 * Check database/where_builder.php for condition writing guide.
 */
function select($from, $fields = '*', $where = [], $order = [], $limit = null, $skip = null) {
    $connection = connect();
    $query = _buildSelectionQueryStatement($connection, $from, $fields, $where, $order, $limit, $skip);
    $result = $connection->query($query);

    if (! $result) {
        throwSqlError($connection);
    } else {
        return _resultToArray($result);
    }
}

/**
 * Execute a SELECT query and return the first matched row.
 * 
 * @param  string        $from
 * @param  string|array  $fields = '*'
 * @param  array         $where = []
 * @param  array         $order = []
 * @return array|null
 * 
 * @example
 *  
 *  selectOne('offices')
 * 
 *  The above call will return the first matched row, in a form of array, of the following SQL: "SELECT * FROM offices 
 *  LIMIT $_CONFIG['default_limit'], $_CONFIG['default_skip']". If there is no matched result, the function
 *  will return null.
 * 
 *  selectOne('offices', ['code', name', 'location'], [['and', 'since', '<', '2018']], ['code' => 'asc'])
 * 
 *  The above call translates to SQL "SELECT code, name, location FROM offices WHERE since < 2018 ORDER BY code ASC 
 *  LIMIT $_CONFIG['default_limit'], $_CONFIG['default_skip']". This function will return the first matched row from the
 *  result set. Null will be returned when there are no matched rows.
 *  
 *  Check database/where_builder.php for condition writing guide.
 */
function selectOne($from, $fields = '*', $where = [], $order = []) {
    $result = select($from, $fields, $where, $order);
    return count($result) === 0 ? null : $result[0];
}

/**
 * PRIVATE FUNCTION
 */
function _resultToArray($result) {
    $array = [];
    foreach ($result as $item) {
        array_push($array, $item);
    }
    return $array;
}

/**
 * PRIVATE FUNCTION
 */
function _buildSelectionQueryStatement($connection, $from, $fields, $where, $order, $limit, $skip) {
    return implode(' ', [
        _buildFieldsStatement($fields),
        _buildFromStatement($from),
        buildWhereStatement($connection, $where),
        _buildOrderStatement($order),
        _buildLimitStatement($limit, $skip),
    ]);
}

/**
 * PRIVATE FUNCTION
 */
function _buildFieldsStatement($fields) {
    if (is_string($fields)) {
        return 'SELECT ' . $fields;
    } else {
        return 'SELECT ' . _normalizeAliasedFields($fields);
    }
}

/**
 * PRIVATE FUNCTION
 */
function _buildFromStatement($from) {
    return 'FROM ' . $from;
}

/**
 * PRIVATE FUNCTION
 */
function _buildOrderStatement($order) {
    if (empty($order)) {
        return '';
    }

    $normalized = array_map(function ($key, $item) {
        return $key . ' ' . $item;
    }, array_keys($order), $order);

    return 'ORDER BY ' . implode(', ', $normalized);
}

/**
 * PRIVATE FUNCTION
 */
function _buildLimitStatement($limit, $skip) {
    global $CONFIG;

    if (is_null($limit)) {
        $limit = $CONFIG['default_limit'];
    }

    if (is_null($skip)) {
        $skip = $CONFIG['default_skip'];
    }

    return 'LIMIT ' . $skip . ',' . $limit; 
}

/**
 * PRIVATE FUNCTION
 */
function _normalizeAliasedFields($fields) {
    $normalized = array_map(function ($key, $item) {
        return is_numeric($key) ? $item : $key . ' AS ' . $item;
    }, array_keys($fields), $fields);

    return implode(', ', $normalized);
}

<?php

/*
 * routes.php
 *
 * This is where every routes definition is written.
 * Every piece of route is represented as an array with these indices and values:
 *
 * string              url      -> The URL in which the route will be accessed when the Request URL matches it.
 * enum('GET', 'POST') method   -> The HTTP Request Method for entering this route.
 * string              action   -> The action/view to be executed when the route is matched.
 * boolean?            view     -> Determine whether this route directly maps to a view. False by default.
 */

return [
    [
        'url' => '',
        'method' => 'GET',
        'action' => 'home',
        //'view' => true,
    ],
    [
        'url' => 'login',
        'method' => 'GET',
        'action' => 'login',
        'view' => true,
    ],
    [
        'url' => 'login',
        'method' => 'POST',
        'action' => 'auth/login',
    ],
    [
        'url' => 'logout',
        'method' => 'GET',
        'action' => 'auth/logout',
    ],
    [
        'url' => 'cart',
        'method' => 'GET',
        'action' => 'shoppingcart',
    ],
    [
        'url' => 'signup',
        'method' => 'GET',
        'action' => 'signup',
        'view' => true,
    ],
    [
        'url' => 'admin',
        'method' => 'GET',
        'action' => 'admin/admin',
        'view' => true,
    ],
    [
        'url' => 'profile',
        'method' => 'GET',
        'action' => 'profile',
        'view' => true,
    ],
    [
        'url' => 'user',
        'method' => 'GET',
        'action' => 'user',
    ],
    [
        'url' => 'order',
        'method' => 'GET',
        'action' => 'order',
    ],
    [
        'url' => 'hot',
        'method' => 'GET',
        'action' => 'hot',
    ],
    [
        'url' => 'admin/food/insert',
        'method' => 'POST',
        'action' => 'admin/insert_food',
    ],
    [
        'url' => 'admin/food/delete',
        'method' => 'POST',
        'action' => 'admin/delete_food',
    ],
    [
        'url' => 'admin/food/update',
        'method' => 'POST',
        'action' => 'admin/update_food',
    ],
    [
        'url' => 'admin/hot/insert',
        'method' => 'POST',
        'action' => 'admin/insert_hot',
    ],
    [
        'url' => 'admin/hot/delete',
        'method' => 'POST',
        'action' => 'admin/delete_hot',
    ],
    [
        'url' => 'admin/hot/update',
        'method' => 'POST',
        'action' => 'admin/update_hot',
    ],
    [
        'url' => 'admin/user/insert',
        'method' => 'POST',
        'action' => 'admin/insert_hot',
    ],
    [
        'url' => 'admin/user/delete',
        'method' => 'POST',
        'action' => 'admin/delete_user',
    ],
    [
        'url' => 'admin/user/update',
        'method' => 'POST',
        'action' => 'admin/update_user',
    ],
    [
        'url' => 'admin/profile/update',
        'method' => 'POST',
        'action' => 'admin/update_profile',
    ],
    [
        'url' => 'admin/order/delete',
        'method' => 'POST',
        'action' => 'admin/delete_order',
    ],
    [
        'url' => 'admin/order/update',
        'method' => 'POST',
        'action' => 'admin/update_order',
    ],
    [
        'url' => 'invoice',
        'method' => 'GET',
        'action' => 'invoice',
    ],
    [
        'url' => 'adminprofile',
        'method' => 'GET',
        'action' => 'admin/profile_admin',
        'view' => true,
    ],
    [
        'url' => 'editprofile',
        'method' => 'GET',
        'action' => 'editprofile',
        'view' => true,
    ],
    [
        'url' => 'editprofile',
        'method' => 'POST',
        'action' => 'auth/editprofile',
    ],
    [
        'url' => 'signup',
        'method' => 'POST',
        'action' => 'auth/signup',
    ],
    [
        'url' => 'api/cart',
        'method' => 'POST',
        'action' => 'api/cart_insert',
    ],
    [
        'url' => 'api/cartr',
        'method' => 'POST',
        'action' => 'api/cart_remove',
    ],
    [
        'url' => 'api/cartd',
        'method' => 'POST',
        'action' => 'api/cart_delete',
    ],
    [
        'url' => 'addresselect',
        'method' => 'GET',
        'action' => 'addressselectcheck',
        //'view' => true,
    ],
    [
        'url' => 'addresselect',
        'method' => 'POST',
        'action' => 'addressselect',
    ],
    [
        'url' => 'orderconfirmation',
        'method' => 'GET',
        'action' => 'orderconfirmation',
    ],
    [
        'url' => 'makeorder',
        'method' => 'POST',
        'action' => 'makeorder',
    ],
    [
        'url' => 'food',
        'method' => 'GET',
        'action' => 'food',
    ],
    [
        'url' => 'foods',
        'method' => 'GET',
        'action' => 'foods',
    ],
    [
        'url' => 'drinks',
        'method' => 'GET',
        'action' => 'drinks',
    ],
    [
        'url' => 'tentangkami',
        'method' => 'GET',
        'action' => 'tentangkami',
        'view' => true,
    ],
];

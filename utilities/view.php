<?php

include_once 'config.php';

/**
 * Include a view of the specified name including the specified data.
 * 
 * @param  string  $name
 * @param  array   $data
 * @return void
 */
function view($name, $data = []) {
    extract($data);
    require_once 'views/' . $name . '.php';
}

/**
 * Include a partial view of the specified name including the specified data.
 * 
 * @param  string  $name
 * @param  array   $data
 * @return void
 */
function partial($name, $data = []) {
    extract($data);
    include 'views/partial/' . $name . '.php';
}

/**
 * Return a proper URL for the given URL string
 * 
 * @param  string  $name
 * @return string
 */
function url($name) {
    global $CONFIG;
    return $CONFIG['root'] . $name;
}

/**
 * Return a proper asset URL for the given URL string
 * 
 * @param  string  $name
 * @return string
 */
function asset($name) {
    return url('assets/' . $name);
}
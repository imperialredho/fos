<?php

include_once 'config.php';

/**
 * Redirect to the given location.
 * 
 * @return void
 */
function redirect($location) {
    global $CONFIG;
	header('Location: '. $CONFIG['root'] . $location);
	exit;
}
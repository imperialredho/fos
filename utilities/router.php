<?php

/**
 * Dispatch a route matched the given URL and request method.
 * 
 * @param  string  $url
 * @param  string  $method
 * @return void
 */
function dispatchRoute($url, $method) {
    $routes = getRoutesDefinition();
    $matched = findMatchedRoute($routes, $url, $method);
    $file = buildFilename($matched);
    $inputs = extractInput($matched);

    requireImportantModules();

    require_once $file;
}

/**
 * Find the matched route of the given URL and request method, in the given routes definition.
 * 
 * @param  array   $routes
 * @param  string  $url
 * @param  string  $method
 * @return array
 */
function findMatchedRoute($routes, $url, $method) {
    return array_filter($routes, function ($route) use ($url, $method) {
        return $route['url'] === $url && $route['method'] === $method;
    });
}

/**
 * Return the route definition in routes.php.
 * 
 * @return array
 */
function getRoutesDefinition() {
    $definitions = (include 'routes.php');
    return $definitions;
}

/**
 * Build a filename based on the matched route.
 * 
 * @param  array  $matched
 * @return string
 */
function buildFilename($matched) {
    if (count($matched) === 0) {
        $file = 'views/404.php';
    } else {
        $route = reset($matched);
        $file = checkDirectness($route['view'] ?? false) . '/' . $route['action'] . '.php';
    }

    if (! file_exists($file)) {
        die('File ' . $file . ' does not exist!');
    }

    return $file;
}

/**
 * Return the proper variables for $inputs based on the matched route.
 * 
 * @param  array  $matched
 * @return array
 */
function extractInput($matched) {
    if (count($matched) === 0) {
        return [];
    } else if (reset($matched)['method'] === 'GET') {
        return $_GET;
    } else if (reset($matched)['method'] === 'POST') {
        return $_POST;
    }
}

/**
 * Require important modules for every actions.
 * 
 * @return void
 */
function requireImportantModules() {
    require_once 'utilities/view.php';
    require_once 'utilities/redirect.php';
    require_once 'utilities/flash.php';
    require_once 'utilities/auth.php';
}

/**
 * Determine the directory based on the given directness.
 * 
 * @param  boolean  $direct
 * @return string
 */
function checkDirectness($direct) {
    return $direct ? 'views' : 'actions';
}

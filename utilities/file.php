<?php

/**
 * Upload file with the given filename into the given directory, and return its uploaded path.
 * 
 * @param  string  $directory
 * @param  string  $file
 * @return string
 */
function upload($directory, $file) {
    $path = $directory . $_FILES[$file]['name'];

    move_uploaded_file($_FILES[$file]['tmp_name'], 'assets/' . $path);

    return $path;
}

/**
 * Determines whether file with the given filename is uploaded in the request.
 * 
 * @param  string  $directory
 * @return boolean
 */
function isUploaded($file) {
    return is_uploaded_file($_FILES[$file]['tmp_name']);
}

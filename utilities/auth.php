<?php

include_once 'utilities/redirect.php';
include_once 'database/select.php';

/**
 * Log the given user to the current session.
 *
 * @param  array  $user
 * @return void
 */
function login($user) {
    $_SESSION['login'] = $user;
}

/**
 * Perform a logout on the current session.
 *
 * @return void
 */
function logout() {
    unset($_SESSION['login']);
}

/**
 * Check whether there is an authenticated user in the current session.
 *
 * @return boolean
 */
function isAuth() {
    return isset($_SESSION['login']);
}

/**
 * Return the currently authenticated user.
 *
 * @return array
 */
function getAuthUser() {
    if (! isAuth()) {
        return [];
    } else {
        $user = $_SESSION['login'];
        $user['cart_count'] = selectOne('groceries', ['SUM(quantity)' => 'cart_count'], [
            ['and', 'email', '=', $user['email']]
        ])['cart_count'];

        return $user;
    }
}

/**
 * Get the privilege of the currently authenticated user.
 *
 * @return null|integer
 */
function getPrivilege() {
    if (! isAuth())
        return null;
    else
        return $_SESSION['login']['privilege'];
}

/**
 * Check whether the authenticated user has the given privilege.
 *
 * @param  integer  $privilege
 * @return boolean
 */
function isPrivilege($privilege) {
    if (! isAuth())
        return false;
    else
        return getPrivilege() == $privilege;
}

/**
 * Guard the action/view to only allow unauthenticated users.
 *
 * @return void
 */
function allowGuestOnly() {
	if (isAuth()) {
        redirect('');
    }
}

/**
 * Guard the action/view to only allow authenticated users.
 *
 * @return void
 */
function allowAuthOnly() {
    if (! isAuth()) {
		redirect('');
    }
}

/**
 * Guard the action/view to only allow authenticated users with the given privilege.
 *
 * @param  array|integer  $privilege
 * @param  string         $redirect
 * @return void
 */
function allowPrivilegedOnly($privilege, $redirect) {
    $privileges = is_numeric($privilege) ? [$privilege] : $privilege;

    if (! in_array(getPrivilege(), $privileges)) {
        redirect($redirect);
    }
}

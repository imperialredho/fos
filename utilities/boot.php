<?php

include_once 'utilities/flash.php';

/**
 * Perform the preparation for the application.
 * 
 * @return void
 */
function boot() {
    session_start();
}

/**
 * Perform the wrap up for the application.
 * 
 * @return void
 */
function unboot() {
    flushFlash();
}
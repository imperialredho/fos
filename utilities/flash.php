<?php

/**
 * Get or set a flash message.
 * 
 * @param  string      $name
 * @param  mixed|null  $value
 * @return mixed
 */
function flash($name, $value = null) {
    return is_null($value) ? 
        getFlashItem($name) : 
        setFlashItem($name, $value);
}

/**
 * Determine whether a flash message with the given name exists.
 * 
 * @param  string  $name
 * @return boolean
 */
function flashExists($name) {
    return isset($_SESSION['flash'][$name]);
}

/**
 * Return a flash message with the given name.
 * 
 * @param  string  $name
 * @return mixed|null
 */
function getFlashItem($name) {
    return $_SESSION['flash'][$name] ?? null;
}

/**
 * Set a flash message with the given name and value.
 * 
 * @param  string  $name
 * @param  mixed   $value
 * @return void
 */
function setFlashItem($name, $value) {
    $_SESSION['flash'][$name] = $value;
}

/**
 * Clear all the flush messages.
 * 
 * @return void
 */
function flushFlash() {
    unset($_SESSION['flash']);
}
<?php

/*
 * config.php
 * 
 * This file stores the global configuration variable for the whole application.
 * The variable, $CONFIG, will be available in every file except in a function.
 * To include it in a function, put "global $CONFIG" on the first line.
 */

$CONFIG = [

    // The root URL of this application
    'root' => 'http://localhost/fos/',

    // Database configurations (MySQL)
    'db_host' => 'localhost',
    'db_username' => 'root',
    'db_password' => '',
    'db_name' => 'db_food_ordering',
    'default_limit' => 30,
    'default_skip' => 0,

];
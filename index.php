<?php

/*
 * FOOD ORDERING SYSTEM
 * Nasi Goreng Padang Pak Arul
 * -----------------------------
 * Authors:
 * 1052160xx Arie
 * 1052160xx Redho
 * 1052160xx Qubay
 * 1052160xx Deta
 * -----------------------------
 * 
 * index.php
 * 
 * This file serves as the entry point of this application.
 * Every request will be directed to this file, so it's this file's responsibility
 * to direct in which way this application will go.
 */

/*
 * First, we include the necessary modules.
 */
include_once 'config.php';
include_once 'utilities/boot.php';
include_once 'utilities/router.php';

/*
 * Call the boot() function which prepares the application's functionality.
 */
boot();

/*
 * We dispatch the route from "url" query and pass the method of the incoming request.
 */
dispatchRoute($_GET['url'], $_SERVER['REQUEST_METHOD']);

/*
 * Finally, we call the unboot() function to wrap up the application before the response is sent.
 */
unboot();

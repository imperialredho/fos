-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 29, 2018 at 02:30 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_food_ordering`
--

-- --------------------------------------------------------

--
-- Table structure for table `groceries`
--

CREATE TABLE `groceries` (
  `id_grocery` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groceries`
--

INSERT INTO `groceries` (`id_grocery`, `email`, `id_product`, `quantity`) VALUES
(14, 'redho.redguard@gmail.com', 25, 3),
(15, 'redho.redguard@gmail.com', 1, 1),
(27, 'admin@admin.com', 25, 4),
(28, 'admin@admin.com', 2, 2),
(29, 'admin@admin.com', 14, 1),
(30, 'admin@admin.com', 13, 1),
(31, 'admin@admin.com', 12, 1),
(32, 'admin@admin.com', 11, 1),
(34, 'admin@admin.com', 8, 1),
(35, 'admin@admin.com', 1, 1),
(36, 'admin@admin.com', 3, 2),
(37, 'admin@admin.com', 4, 2),
(38, 'admin@admin.com', 22, 2),
(39, 'admin@admin.com', 7, 2),
(40, 'admin@admin.com', 6, 2),
(41, 'admin@admin.com', 5, 2),
(42, 'admin@admin.com', 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id_order` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `delivery_fee` int(11) NOT NULL,
  `note` varchar(128) DEFAULT NULL,
  `total_fee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id_order`, `created_at`, `address`, `email`, `delivery_fee`, `note`, `total_fee`) VALUES
(31, '2018-11-17 16:00:49', 'Jalan Matraman Raya, Jakarta Timur, Matraman, Jakarta, DKI Jakarta, 13150', 'redho.redguard@gmail.com', 108000, 'nasi setengah', 0),
(32, '2018-11-17 16:07:56', 'Jalan Letjen. Mt. Haryono, Jakarta Selatan, Pancoran, Jakarta, DKI Jakarta, 12780', 'redho.redguard@gmail.com', 90000, '', 0),
(33, '2018-11-17 16:35:32', 'Jalan Utan Kayu 33, Jakarta Timur, Matraman, Jakarta, DKI Jakarta, 13130', 'redho.redguard@gmail.com', 30000, '', 0),
(34, '2018-11-18 07:57:36', 'Jalan Simprug Garden 3, Grogol Selatan, Kebayoran Lama, Jakarta, DKI Jakarta, 12220', 'cust@mail.com', 33000, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id_order` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id_order`, `id_product`, `quantity`) VALUES
(31, 1, 1),
(31, 3, 5),
(32, 2, 1),
(32, 3, 1),
(32, 4, 1),
(32, 7, 2),
(32, 10, 1),
(33, 4, 2),
(34, 3, 1),
(34, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id_product` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `price` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `photo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id_product`, `name`, `description`, `price`, `type`, `photo`) VALUES
(1, 'Soto Minang', NULL, 18000, 1, 'img/soto_minang.png'),
(2, 'Nasi Goreng Padang Biasa', NULL, 13000, 1, 'img/nasi_goreng_biasa.png'),
(3, 'Nasi Goreng Padang Daging', NULL, 18000, 1, 'img/nasi_goreng_daging.png'),
(4, 'Nasi Goreng Padang Pete', NULL, 15000, 1, 'img/nasi_goreng_pete.png'),
(5, 'Mie Goreng Padang Biasa', NULL, 13000, 1, 'img/mie_goreng.png'),
(6, 'Mie Goreng Padang Pete', NULL, 15000, 1, 'img/mie_goreng_pete.png'),
(7, 'Mie Goreng Padang Daging', NULL, 18000, 1, 'img/mie_goreng_daging.png'),
(8, 'Teh Telur', NULL, 8000, 2, 'img/teh_telur.png'),
(10, 'Kopi Telur', NULL, 8000, 2, 'img/kopi_telur.png'),
(11, 'Nutrisari Telur', NULL, 8000, 2, 'img/nutrisari_telur.png'),
(12, 'Capuccino', NULL, 10000, 2, 'img/cappucino.png'),
(13, 'Es Jeruk', NULL, 8000, 2, 'img/es_jeruk.png'),
(14, 'Es Teh Manis', NULL, 4000, 2, 'img/es_teh_manis.png'),
(22, 'Soto', NULL, 18000, 1, 'img/soto.png'),
(25, 'Kwietiaw', NULL, 18000, 1, 'img/kwetiauw_goreng.png');

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

CREATE TABLE `promotions` (
  `id_hot` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image-sm` varchar(255) NOT NULL,
  `image-md` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promotions`
--

INSERT INTO `promotions` (`id_hot`, `id_product`, `image`, `image-sm`, `image-md`) VALUES
(5, 25, 'img/promoKwi.png', '', ''),
(6, 10, 'img/promoKoT.png', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `privilege` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`email`, `password`, `name`, `phone`, `privilege`) VALUES
('admin@admin.com', 'no', 'admin', '0888288828', 1),
('cust@mail.com', 'baru', 'Customer', '08080808080', 2),
('redho.redguard@gmail.com', 'kuantumq', 'Redho', '089898989899', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groceries`
--
ALTER TABLE `groceries`
  ADD PRIMARY KEY (`id_grocery`),
  ADD KEY `email` (`email`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `email_idx` (`email`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id_product`);

--
-- Indexes for table `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id_hot`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groceries`
--
ALTER TABLE `groceries`
  MODIFY `id_grocery` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id_hot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `groceries`
--
ALTER TABLE `groceries`
  ADD CONSTRAINT `groceries_email` FOREIGN KEY (`email`) REFERENCES `users` (`email`),
  ADD CONSTRAINT `groceries_id_product` FOREIGN KEY (`id_product`) REFERENCES `products` (`id_product`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `email` FOREIGN KEY (`email`) REFERENCES `users` (`email`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`id_order`) REFERENCES `orders` (`id_order`),
  ADD CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`id_product`) REFERENCES `products` (`id_product`);

--
-- Constraints for table `promotions`
--
ALTER TABLE `promotions`
  ADD CONSTRAINT `promotions_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `products` (`id_product`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

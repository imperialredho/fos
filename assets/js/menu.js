(function(){

const rootUrl = document.querySelector('meta[name="root-url"]').content
const cartCount = document.querySelector('.cart-count')

const itemChoose = document.getElementsByClassName('item-choose')
for (let item of itemChoose) {
	item.addEventListener('click', async () => {		
		const data = new URLSearchParams()
		
		data.append('id_product', item.dataset.id)

		const url = rootUrl + 'api/cart'
		const init = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			},
			body: data
		}

		// Show spinner on button when ordering
		makeLoading(item)

		let response = await fetch(url, init)
		let json = await response.json()

		// Done ordering.. put back to original text and show modal
		$('#product-ordered')[0].querySelector('.product-name').textContent = item.dataset.name
		$('#product-ordered').modal('show')
		makeOriginal(item)

		// Increase cart count in nav
		increaseCartCount()
		
	})
}

function makeLoading(button) {
	const spinner = document.createElement('span')
	spinner.classList.add('fas', 'fa-spinner', 'fa-spin')
	spinner.style.marginRight = '5px'

	const loadingText = document.createTextNode('Memesan')

	massacreChildren(button)
	button.appendChild(spinner)
	button.appendChild(loadingText)
}

function makeOriginal(button) {
	const originalText = document.createTextNode('Pilih')

	massacreChildren(button)
	button.appendChild(originalText)
}

function increaseCartCount() {
	cartCount.textContent = parseInt(cartCount.textContent) + 1
}

function massacreChildren(element) {
	while (element.firstChild) {
		element.removeChild(element.firstChild)
	}
}

})()
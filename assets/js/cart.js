(function() {

const rootUrl = document.querySelector('meta[name="root-url"]').content
const cartCount = document.querySelector('.cart-count')
const jumlahUps = document.getElementsByClassName('jumlah-up')
const jumlahDowns = document.getElementsByClassName('jumlah-down')
const deletes = document.getElementsByClassName('delete')
const orderButton = document.querySelector('.btn-order')

const disableEvent = (e) => e.preventDefault()

alterOrderAbility()

for (let jumlahUp of jumlahUps) {
	jumlahUp.addEventListener('click', async (e) => {
		const column = e.target.closest('.shopping-item')
		const jumlahText = column.querySelectorAll('.jumlah-text')
		const data = new URLSearchParams()
		data.append('id_product', jumlahUp.dataset.id)

		const url = rootUrl + 'api/cart'
		const init = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			},
			body: data
		}

		// Show spinner on button when ordering
		makeLoading(jumlahUp)

		let response = await fetch(url, init)
		let json = await response.json()

		makeOriginal(jumlahUp, 'fa-chevron-up')

		jumlahText.forEach(item => item.textContent = parseInt(item.textContent) + 1)

		increaseCartCount()
	})
}

for (let jumlahDown of jumlahDowns) {
	jumlahDown.addEventListener('click', async (e) => {
		const column = e.target.closest('.shopping-item')
		const jumlahText = column.querySelectorAll('.jumlah-text')
		const data = new URLSearchParams()
		data.append('id_product', jumlahDown.dataset.id)

		const url = rootUrl + 'api/cartr'
		const init = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			},
			body: data
		}

		makeLoading(jumlahDown)

		let response = await fetch(url, init)
		let json = await response.json()

		makeOriginal(jumlahDown, 'fa-chevron-down')

		decreaseCartCount(1)

		if (parseInt(jumlahText[0].textContent) > 1){
			jumlahText.forEach(item => item.textContent = parseInt(item.textContent) - 1)
		} else {
			$(column).slideUp(() => column.remove())
		}

		alterOrderAbility()
	})
}

for (let deleted of deletes) {
	deleted.addEventListener('click', async (e) => {
		const column = e.target.closest('.shopping-item')
		const jumlahText = column.querySelector('.jumlah-text')

		const data = new URLSearchParams()
		data.append('id_product', deleted.dataset.id)

		const url = rootUrl + 'api/cartd'
		const init = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			},
			body: data
		}

		makeLoading(deleted)

		let response = await fetch(url, init)
		let json = await response.json()

		$(column).slideUp(() => {
			decreaseCartCount(parseInt(jumlahText.innerHTML))
			column.remove()
		})

		alterOrderAbility()
	})
}

function makeLoading(button) {
	const spinner = document.createElement('i')
	spinner.classList.add('fas', 'fa-spinner', 'fa-spin')

	massacreChildren(button)
	button.appendChild(spinner)
}

function makeOriginal(button, type) {
	const original = document.createElement('i')
	original.classList.add('fas', type)

	massacreChildren(button)
	button.appendChild(original)
}

function increaseCartCount() {
	cartCount.textContent = parseInt(cartCount.textContent) + 1
}

function decreaseCartCount(amount) {
	let newAmount = parseInt(cartCount.textContent) - amount
	cartCount.textContent = newAmount < 0 ? 0 : newAmount
}

function massacreChildren(element) {
	while (element.firstChild) {
		element.removeChild(element.firstChild)
	}
}

function alterOrderAbility() {
	if (parseInt(cartCount.textContent) === 0)
		orderButton.addEventListener('click', disableEvent, false)
	else
		orderButton.removeEventListener('click', disableEvent, false)
}

})()

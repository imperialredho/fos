(() => {

  const deleteButtons = document.getElementsByClassName('product-delete');
  const updateButtons = document.getElementsByClassName('product-update');
  const hiddenInput = document.getElementById('order-id');
  const hiddenInputUpdate = document.getElementById('order-id-update');
  const addressUpdate = document.getElementById('inputAddress');
  const feeUpdate = document.getElementById('inputFee');
  const feeUpdate2 = document.getElementById('inputFee2');
  const noteUpdate = document.getElementById('inputNote');
  const dateUpdate = document.getElementById('inputDate');
  const emailUpdate = document.getElementById('inputEmail');

  for (let button of deleteButtons) {
    button.addEventListener('click', () => {
      hiddenInput.value = button.dataset.id
    })
  }

  for (let button of updateButtons) {
    button.addEventListener('click', () => {
      hiddenInputUpdate.value = button.dataset.id
      addressUpdate.value = button.dataset.address
      feeUpdate.value = button.dataset.fee
      feeUpdate2.value = button.dataset.fee2
      noteUpdate.value = button.dataset.note
      dateUpdate.value = button.dataset.created
      emailUpdate.value = button.dataset.email
    })
  }

  })()

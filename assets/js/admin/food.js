(() => {

  const deleteButtons = document.getElementsByClassName('product-delete');
  const updateButtons = document.getElementsByClassName('product-update');
  const hiddenInput = document.getElementById('product-id');
  const hiddenInputUpdate = document.getElementById('product-id-update');
  const menuUpdate = document.getElementById('product-menu');
  const descUpdate = document.getElementById('update-desc');
  const priceUpdate = document.getElementById('product-price');
  const typeUpdate = document.getElementById('product-type');
  const imgUpdate = document.getElementById('product-img');

  for (let button of deleteButtons) {
    button.addEventListener('click', () => {
      hiddenInput.value = button.dataset.id
    })
  }

  for (let button of updateButtons) {
    button.addEventListener('click', () => {
      hiddenInputUpdate.value = button.dataset.id
      menuUpdate.value = button.dataset.name
      descUpdate.value = button.dataset.desc
      priceUpdate.value = button.dataset.price
      typeUpdate.value = button.dataset.type
      imgUpdate.src = button.dataset.img
    })
  }

})()

(() => {
  const deleteButtons = document.getElementsByClassName('user-delete');
  const hiddenInput = document.getElementById('user-email-key');
  const updateButtons = document.getElementsByClassName('user-update');
  const hiddenInputUser = document.getElementById('product-id-update');
  const nameUser = document.getElementById('product-name');
  const emailUser = document.getElementById('product-email');
  const passwordUser = document.getElementById('product-pass');
  const phoneUser = document.getElementById('product-phone');
  const priUser = document.getElementById('product-pri');

  for (let button of deleteButtons) {
    button.addEventListener('click', () => {
      hiddenInput.value = button.dataset.id
    })
  }

  for (let button of updateButtons) {
    button.addEventListener('click', () => {
      hiddenInputUser.value = button.dataset.id
      nameUser.value = button.dataset.name
      emailUser.value = button.dataset.id
      passwordUser.value = button.dataset.password
      phoneUser.value = button.dataset.phone
      priUser.value = button.dataset.privilege
    })
  }

  })()

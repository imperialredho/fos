(() => {

  const deleteButtons = document.getElementsByClassName('hot-delete');
  const updateButtons = document.getElementsByClassName('hot-update');
  const hiddenInput = document.getElementById('hot-id');
  const hiddenInputUpdate = document.getElementById('hot-id-update');
  const productsUpdate = document.getElementById('product-menu-update');
  const imgUpdate = document.getElementById('inputImage');

  for (let button of deleteButtons) {
    button.addEventListener('click', () => {
      hiddenInput.value = button.dataset.id
    })
  }

  for (let button of updateButtons) {
    button.addEventListener('click', () => {
      hiddenInputUpdate.value = button.dataset.id
      productsUpdate.value = button.dataset.idproduct
      imgUpdate.src = button.dataset.img
    })
  }

})()

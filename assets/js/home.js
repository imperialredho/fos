(function(){


const rootUrl = document.querySelector('meta[name="root-url"]').content
const cartCount = document.querySelector('.cart-count')

const hotItem = document.getElementsByClassName('hot')
for (let hot of hotItem) {
	hot.addEventListener('click', async () => {		
		const data = new URLSearchParams()
		
		data.append('id_product', hot.dataset.id)

		const url = rootUrl + 'api/cart'
		const init = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			},
			body: data
		}

		// Show spinner on button when ordering

		let response = await fetch(url, init)
		let json = await response.json()
	
		$('#product-ordered').modal('show')
	
		// Increase cart count in nav
		increaseCartCount()
		
	})
}

function increaseCartCount() {
	cartCount.textContent = parseInt(cartCount.textContent) + 1
}


})()
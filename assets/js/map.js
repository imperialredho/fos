(function() {

var mymap = L.map('map').setView([-6.21462, 106.84513], 13);

const finalAddress = document.querySelector('.address');

const addressMapInput = document.querySelector('.addressMapInput');

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoicmVkaG9kYXJtYXdhbiIsImEiOiJjam9pcXBrdnYwYzU5M3BsY3Y3NHF0MXEyIn0.y_cErrvlboEFZHK9uHNYQQ'
}).addTo(mymap);

var searchControl = L.esri.Geocoding.geosearch().addTo(mymap);

var geocodeService = L.esri.Geocoding.geocodeService();

var layerGroup = L.layerGroup().addTo(mymap);

mymap.on('dblclick', function(e) {
	geocodeService.reverse().latlng(e.latlng).run(function(error, result) {
		layerGroup.clearLayers();
	  	L.marker(result.latlng).addTo(layerGroup).bindPopup(result.address.Match_addr).openPopup();
	  	finalAddress.innerHTML = result.address.Match_addr
	  	addressMapInput.value = result.address.Match_addr
	});
});

})()

